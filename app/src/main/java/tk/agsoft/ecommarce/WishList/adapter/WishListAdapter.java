package tk.agsoft.ecommarce.WishList.adapter;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import tk.agsoft.ecommarce.DTO.ProductDTO;
import tk.agsoft.ecommarce.Login.LoginFragment;
import tk.agsoft.ecommarce.ProductDetail.ProductDetailFragment;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.WishList.WishListFragment;
import tk.agsoft.ecommarce.WishList.service.WishListSingleToneService;
import tk.agsoft.ecommarce.WishList.service.WishlistService;
import tk.agsoft.ecommarce.common.DataGetterSetter;

public class WishListAdapter extends RecyclerView.Adapter <WishListAdapter.WishlistHolderBind> {

    private Context context;
    private static List<ProductDTO> wishlistitems;
    private WishlistService wishlistService;
    private RecyclerView prodListRecyclerView;

    public WishListAdapter(Context context, List<ProductDTO> wishlistitems, RecyclerView prodListRecyclerView) {
        this.context = context;
        this.wishlistitems = wishlistitems;
        this.wishlistService = new WishlistService();
        this.prodListRecyclerView = prodListRecyclerView;
    }

    @NonNull
    @Override
    public WishlistHolderBind onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_product_list_card, parent,false);
        return new WishlistHolderBind(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final WishlistHolderBind holder, int position) {

        final ProductDTO productDTO = wishlistitems.get(position);

        holder.id = productDTO.getId();
        holder.productDTO = productDTO;
        if (!productDTO.getProductImage().equals("") && !productDTO.getProductImage().isEmpty()) {
            Picasso
                    .get()
                    .load(Uri.parse(productDTO.getProductImage()))
                    .placeholder(R.drawable.iconfinder_32_171485)
                    .error(R.drawable.ic_error_black_24dp)
                    .resize(130, 130)
                    .centerCrop()
                    .into(holder.prodImage);

        }

        holder.prodName.setText(productDTO.getProductName());

        if (productDTO.getProductRating() == 0.0 || productDTO.getProductRating() == null) {
            holder.prodRating.setBackgroundColor(context.getResources().getColor(R.color.White));
            holder.prodRating.setTextColor(context.getResources().getColor(R.color.Black));
        } else if (productDTO.getProductRating() >= 0.1 && productDTO.getProductRating() <= 2.5) {
            holder.prodRating.setBackground(context.getResources().getDrawable(R.drawable.red_rounded_corner));
        } else if (productDTO.getProductRating() > 2.5 && productDTO.getProductRating() <= 3.9) {
            holder.prodRating.setBackground(context.getResources().getDrawable(R.drawable.yellow_rounded_corner));
        } else if (productDTO.getProductRating() >= 4.0 && productDTO.getProductRating() <= 5.0) {
            holder.prodRating.setBackground(context.getResources().getDrawable(R.drawable.green_rounded_corner));
        }

        if (productDTO.getProductRating() == 0.0 || productDTO.getProductRating() == null)
            holder.prodRating.setText("Not Yet Rated");
        else
            holder.prodRating.setText(productDTO.getProductRating().toString());
        holder.prodPrice.setText(productDTO.getProductPrice().toString());

        if (WishListSingleToneService.getmInstance().getWishListItem(productDTO)) {
            holder.whatToDo = false;
            holder.prodWishListIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.wishlist_icon));
        } else {
            holder.whatToDo = true;
            holder.prodWishListIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.non_wishlist_icon));
        }

        holder.productListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductDetailFragment productDetail = new ProductDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("ProductId", holder.id);
                productDetail.setArguments(bundle);

                FragmentTransaction fragmentTransaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.homeContainer, productDetail, "ProductDetailContainer").addToBackStack("ProductDetailContainer");
                fragmentTransaction.commit();
            }
        });

        holder.prodWishListIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String token = DataGetterSetter.getmInstance(context).getInfo().getToken();

                if (token != null && !token.isEmpty()) {
                    if (holder.whatToDo) {
                        wishlistService.addToWishlist(context, holder.id, prodListRecyclerView);
                        holder.whatToDo = false;
                    } else {
                        wishlistService.removeFromWishlist(context, holder.productDTO, prodListRecyclerView);
                        holder.whatToDo = true;
                    }

                } else {
                    LoginFragment loginFragment = new LoginFragment();
                    FragmentTransaction fragmentTransaction = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragmentContainer, loginFragment).addToBackStack("LoginContainer");
                    fragmentTransaction.commit();
                }

            }
        });
    }

    @Override
    public int getItemCount() {

        if (wishlistitems.size() == 0)
            WishListFragment.wishNote.setVisibility(View.VISIBLE);

        return wishlistitems.size();
    }

    class WishlistHolderBind extends RecyclerView.ViewHolder{

        ImageView prodImage,prodWishListIcon;
        TextView prodName, prodRating,prodPrice;
        CardView productListItem;
        boolean whatToDo;
        Integer id;
        ProductDTO productDTO;

        WishlistHolderBind(View view) {
            super(view);

            prodImage = view.findViewById(R.id.product_image);
            prodName = view.findViewById(R.id.product_name);
            prodRating = view.findViewById(R.id.product_rating);
            prodPrice = view.findViewById(R.id.product_price);
            prodWishListIcon = view.findViewById(R.id.product_wishlist_icon);
            productListItem = view.findViewById(R.id.product_list_item);
            whatToDo = false;
            productDTO = new ProductDTO();
        }
    }

    public void swap(List<ProductDTO> list){
        if (wishlistitems != null) {
            wishlistitems.clear();
            wishlistitems.addAll(list);
        }
        else {
            wishlistitems = list;
        }
        notifyDataSetChanged();
    }
}
