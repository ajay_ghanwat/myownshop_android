package tk.agsoft.ecommarce.WishList;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import tk.agsoft.ecommarce.DTO.UserDetailDTO;
import tk.agsoft.ecommarce.Login.LoginFragment;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.WishList.service.WishListSingleToneService;
import tk.agsoft.ecommarce.WishList.service.WishlistService;
import tk.agsoft.ecommarce.common.DataGetterSetter;
import tk.agsoft.ecommarce.common.ProgressDialogBar;

public class WishListFragment extends Fragment {

    View view;
    public static TextView wishNote;
    RecyclerView wishLists;
    WishlistService wishlistService;
    LinearLayout notLoginContainer, wishListShowContainer;
    Button userWantLogin;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_wishlist, container, false);
        initialize(view);
        return view;
    }

    private void initialize(View view) {

        wishNote = view.findViewById(R.id.user_wish_note);
        wishLists = view.findViewById(R.id.user_wishLists);
        wishLists.setLayoutManager(new LinearLayoutManager(getContext()));

        notLoginContainer = view.findViewById(R.id.userNotLogin);
        wishListShowContainer = view.findViewById(R.id.wishListShowContainer);

        wishlistService = new WishlistService();

        userWantLogin = view.findViewById(R.id.userWantLogin);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onStart() {
        super.onStart();

        userWantLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginFragment loginFragment = new LoginFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragmentContainer, loginFragment).addToBackStack("LoginContainer");
                fragmentTransaction.commit();
            }
        });

        UserDetailDTO userDetailDTO = DataGetterSetter.getmInstance(getContext()).getInfo();

        if(userDetailDTO.getToken() != null && userDetailDTO.getId() != null) {
            ProgressDialogBar.getmInstance(getContext()).showDialog();
            wishlistService.getWishlist(getContext(), wishLists);
            notLoginContainer.setVisibility(View.GONE);

            if (WishListSingleToneService.getmInstance().getCount() > 0 ) {
                wishLists.setVisibility(View.VISIBLE);
                wishNote.setVisibility(View.GONE);
            } else {
                wishLists.setVisibility(View.GONE);
                wishNote.setVisibility(View.VISIBLE);
            }
        } else {
            wishListShowContainer.setVisibility(View.GONE);
        }

    }

}
