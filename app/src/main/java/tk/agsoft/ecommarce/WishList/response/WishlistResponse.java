package tk.agsoft.ecommarce.WishList.response;

import tk.agsoft.ecommarce.DTO.WishlistDTO;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class WishlistResponse extends Response {

	private WishlistDTO wishlists;
	private WishlistDTO addedWish;

	public WishlistDTO getWishlists() {
		return wishlists;
	}

	public void setWishlists(WishlistDTO wishlists) {
		this.wishlists = wishlists;
	}

	public WishlistDTO getAddedWish() {
		return addedWish;
	}

	public void setAddedWish(WishlistDTO addedWish) {
		this.addedWish = addedWish;
	}

}
