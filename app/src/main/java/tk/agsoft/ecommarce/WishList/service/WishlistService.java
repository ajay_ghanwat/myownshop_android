package tk.agsoft.ecommarce.WishList.service;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.DTO.ProductDTO;
import tk.agsoft.ecommarce.DTO.UserDetailDTO;
import tk.agsoft.ecommarce.WishList.adapter.WishListAdapter;
import tk.agsoft.ecommarce.common.DataGetterSetter;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.ProgressDialogBar;
import tk.agsoft.ecommarce.common.RunTimeComponents;
import tk.agsoft.ecommarce.WishList.request.WishListRequest;
import tk.agsoft.ecommarce.WishList.response.WishlistResponse;

public class WishlistService {

    public void getWishlist(final Context context, final RecyclerView wishLists){

        UserDetailDTO userDetailDTO = DataGetterSetter.getmInstance(context).getInfo();

        WishListRequest wishListRequest = new WishListRequest();
        wishListRequest.setLoggedInUserType(userDetailDTO.getUserType());
        wishListRequest.setUserId(userDetailDTO.getId());

        final Call<WishlistResponse> response = NetworkCall.getmInstance().wishlistInterface().getWishlist(userDetailDTO.getToken(), wishListRequest);

        response.enqueue(new Callback<WishlistResponse>() {
            @Override
            public void onResponse(Call<WishlistResponse> call, Response<WishlistResponse> response) {

                ProgressDialogBar.getmInstance(context).hideDialog();

                if (response.body().getStatus() == ResponseStatus.OK){

                    WishListSingleToneService.getmInstance().addMultipleWishList(response.body().getWishlists().getProdId());

                    wishLists.setAdapter(new WishListAdapter(context, WishListSingleToneService.getmInstance().getAProductList(), wishLists));

                } else {
                    RunTimeComponents.showAlertDialogBox(context, null, response.body().getDescription());
                }
            }

            @Override
            public void onFailure(Call<WishlistResponse> call, Throwable t) {
                ProgressDialogBar.getmInstance(context).hideDialog();
                RunTimeComponents.showAlertDialogBox(context, null, "Something Went wrong");
            }
        });

    }

    public void addToWishlist(final Context context, Integer prodId, final RecyclerView prodListRecyclerView){

        UserDetailDTO userDetailDTO = DataGetterSetter.getmInstance(context).getInfo();

        WishListRequest wishListRequest = new WishListRequest();
        wishListRequest.setLoggedInUserType(userDetailDTO.getUserType());
        wishListRequest.setProdId(prodId);
        wishListRequest.setUserId(userDetailDTO.getId());

        final Call<WishlistResponse> response = NetworkCall.getmInstance().wishlistInterface().addWishlist(userDetailDTO.getToken(), wishListRequest);

        response.enqueue(new Callback<WishlistResponse>() {
            @Override
            public void onResponse(Call<WishlistResponse> call, Response<WishlistResponse> response) {
                ProgressDialogBar.getmInstance(context).hideDialog();
                if (response.body().getStatus() == ResponseStatus.OK){

                    WishListSingleToneService.getmInstance().addMultipleWishList(response.body().getAddedWish().getProdId());
                    Toast.makeText(context, "Added To WishList", Toast.LENGTH_SHORT).show();

                    if (prodListRecyclerView != null)
                        prodListRecyclerView.getAdapter().notifyDataSetChanged();

                } else {
                    RunTimeComponents.showAlertDialogBox(context, null, response.body().getDescription());
                }
            }

            @Override
            public void onFailure(Call<WishlistResponse> call, Throwable t) {
                ProgressDialogBar.getmInstance(context).hideDialog();
                RunTimeComponents.showAlertDialogBox(context, null, "Something Went wrong");
            }
        });

    }

    public void removeFromWishlist(final Context context, final ProductDTO productDTO, final RecyclerView prodListRecyclerView){

        UserDetailDTO userDetailDTO = DataGetterSetter.getmInstance(context).getInfo();

        WishListRequest wishListRequest = new WishListRequest();
        wishListRequest.setLoggedInUserType(userDetailDTO.getUserType());
        wishListRequest.setProdId(productDTO.getId());
        wishListRequest.setUserId(userDetailDTO.getId());

        Call<WishlistResponse> response = NetworkCall.getmInstance().wishlistInterface().leftWishlist(userDetailDTO.getToken(), wishListRequest);

        response.enqueue(new Callback<WishlistResponse>() {
            @Override
            public void onResponse(Call<WishlistResponse> call, Response<WishlistResponse> response) {
                ProgressDialogBar.getmInstance(context).hideDialog();
                if (response.body().getStatus() == ResponseStatus.OK){

                    boolean isRemoved = WishListSingleToneService.getmInstance().removeFromWishList(productDTO);

                    if (!isRemoved) {
                        Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }

                    Toast.makeText(context, "Remove From WishList", Toast.LENGTH_SHORT).show();

                    if (prodListRecyclerView != null)
                        prodListRecyclerView.getAdapter().notifyDataSetChanged();

                } else {
                    RunTimeComponents.showAlertDialogBox(context, null, response.body().getDescription());
                }
            }

            @Override
            public void onFailure(Call<WishlistResponse> call, Throwable t) {
                ProgressDialogBar.getmInstance(context).hideDialog();
                RunTimeComponents.showAlertDialogBox(context, null, "Something Went wrong");
            }
        });
    }

}
