package tk.agsoft.ecommarce.WishList.networkInterface;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import tk.agsoft.ecommarce.WishList.request.WishListRequest;
import tk.agsoft.ecommarce.WishList.response.WishlistResponse;

public interface WishlistInterface {

    @POST("getWishlist")
    Call<WishlistResponse> getWishlist(@Header("User-Token") String token, @Body WishListRequest wishListRequest);

    @POST("addWishlist")
    Call<WishlistResponse> addWishlist(@Header("User-Token") String token, @Body WishListRequest wishListRequest);

    @POST("leftWishlist")
    Call<WishlistResponse> leftWishlist(@Header("User-Token") String token, @Body WishListRequest wishListRequest);
}
