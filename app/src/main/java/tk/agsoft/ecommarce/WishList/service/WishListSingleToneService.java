package tk.agsoft.ecommarce.WishList.service;

import android.util.Log;

import java.util.List;

import tk.agsoft.ecommarce.DTO.ProductDTO;
import tk.agsoft.ecommarce.Models.WishList;
import tk.agsoft.ecommarce.WishList.adapter.WishListAdapter;

public class WishListSingleToneService {

    private static WishListSingleToneService mInstance;
    private WishList wishLists;
    private WishListAdapter adapter;

    private WishListSingleToneService() {

        if (wishLists == null) {
            wishLists = new WishList();
        }
    }

    public static synchronized WishListSingleToneService getmInstance() {
        if (mInstance == null) {
            mInstance = new WishListSingleToneService();
        }

        return mInstance;
    }

    public boolean getWishListItem(ProductDTO productDTO) {
        Log.i("Wishlist", wishLists.getProductDTOS().size() + " ");
        if(wishLists.getProductDTOS() != null) {

            for (ProductDTO productDTO1: wishLists.getProductDTOS()){
                if (productDTO1.getId().equals(productDTO.getId()))
                    return true;
            }

        }
        Log.i("Wishlist", wishLists.getProductDTOS().size() + " ");
        return false;
    }

    public boolean addMultipleWishList(List<ProductDTO> list) {

        Log.i("Wishlist", wishLists.getProductDTOS().size() + " ");
        for (ProductDTO productDTO : list) {
            for (ProductDTO productDTO1: wishLists.getProductDTOS()){
                if (productDTO1.getId().equals(productDTO.getId())) {
                    return false;
                }
            }
            wishLists.getProductDTOS().add(productDTO);
        }

        Log.i("Wishlist", wishLists.getProductDTOS().size() + " ");
        return true;
    }

    public boolean removeFromWishList(ProductDTO productDTO) {

        Log.i("Wishlist", wishLists.getProductDTOS().size() + " ");

        for (ProductDTO product : wishLists.getProductDTOS()) {
            if (productDTO.getId().equals(product.getId())) {
                wishLists.getProductDTOS().remove(product);
                return true;
            }
        }

        Log.i("Wishlist", wishLists.getProductDTOS().size() + " ");
        return false;
    }

    public Integer getCount() {
        return wishLists.getProductDTOS().size();
    }

    public List<ProductDTO> getAProductList(){
        return wishLists.getProductDTOS();
    }

    public void clearWishList() {

        wishLists.getProductDTOS().clear();
    }
}
