package tk.agsoft.ecommarce.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SMSReceivers extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();
        SmsMessage[] smsm = null;
        String sms_str ="";

        if (bundle != null)
        {
            Pattern pattern = Pattern.compile("(\\d{4})");

            Object[] pdus = (Object[]) bundle.get("pdus");
            smsm = new SmsMessage[pdus.length];
            for (int i=0; i<smsm.length; i++){
                smsm[i] = SmsMessage.createFromPdu((byte[])pdus[i]);

                sms_str += "\r\nMessage: ";
                sms_str += smsm[i].getMessageBody().toString();
                sms_str+= "\r\n";

                String Sender = smsm[i].getOriginatingAddress();

                if(Sender.equals("+15419914680")) {

                    Matcher matcher = pattern.matcher(sms_str);
                    String val = "";
                    if (matcher.find()) {
                        val = matcher.group(1);
                    }

                    Intent smsIntent = new Intent("otp");
                    smsIntent.putExtra("messageCode",val);

                    LocalBroadcastManager.getInstance(context).sendBroadcast(smsIntent);

                }

            }
        }
    }
}