package tk.agsoft.ecommarce.ServerRequest;

public class Request {

	private String username;
	private Integer userId;
	private String loggedInUserType;
	private String ipAddress;
	private String loginTime;
	private Integer prodId;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLoggedInUserType() {
		return loggedInUserType;
	}

	public void setLoggedInUserType(String loggedInUserType) {
		this.loggedInUserType = loggedInUserType;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public Integer getProdId() {
		return prodId;
	}

	public void setProdId(Integer prodId) {
		this.prodId = prodId;
	}

}