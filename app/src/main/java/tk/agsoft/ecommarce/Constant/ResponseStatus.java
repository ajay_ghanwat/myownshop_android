package tk.agsoft.ecommarce.Constant;

public enum ResponseStatus {
	OK,
	ERROR,
	BADREQUEST,
    UNAUTHORISED,
    ACCESSDENIED,
    EXCEPTION;
}
