package tk.agsoft.ecommarce.Constant;

import java.util.HashMap;
import java.util.Map;

public enum CodeType {

	Verify_User_Registration("Verify_User_Registration"),
	Reset_User_Password("Reset_User_Password");
	
	private String code;
	private static Map<String, CodeType> mapByCode = new HashMap<String, CodeType>();

	static {
		for (CodeType type : CodeType.values()) {
			mapByCode.put(type.code, type);
		}
	}

	private CodeType(String code) {
		this.code = code;
	}

	public String code() {
		return this.code;
	}

	public static CodeType getByVersionCode(String code) {
		return mapByCode.get(code);
	}
}
