package tk.agsoft.ecommarce.Home.service;

import java.util.ArrayList;
import java.util.List;

import tk.agsoft.ecommarce.DTO.ProductDTO;
import tk.agsoft.ecommarce.Home.adapter.ProductListAdapter;

public class ProductSingleToneService {

    private static ProductSingleToneService mInstance;
    private List<ProductDTO> productDTOs;
    private ProductListAdapter adapter;

    private ProductSingleToneService() {

        if (productDTOs == null) {
            productDTOs = new ArrayList<>();
        }
    }

    public static synchronized ProductSingleToneService getmInstance() {
        if (mInstance == null) {
            mInstance = new ProductSingleToneService();
        }

        return mInstance;
    }

    public boolean addMultipleProductItems(List<ProductDTO> list) {

        for (ProductDTO productDTO : list) {
            for (ProductDTO productDTO1: productDTOs){
                if (productDTO1.getId().equals(productDTO.getId())) {
                    return false;
                }
            }
            productDTOs.add(productDTO);
        }
        return true;
    }

    public List<ProductDTO> getProductDTOs() {
        return productDTOs;
    }

}