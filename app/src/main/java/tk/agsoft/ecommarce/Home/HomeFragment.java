package tk.agsoft.ecommarce.Home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import tk.agsoft.ecommarce.Cart.CartFragment;
import tk.agsoft.ecommarce.Filter.FilterFragment;
import tk.agsoft.ecommarce.Home.service.ProductListService;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.common.ProgressDialogBar;

public class HomeFragment extends Fragment {

    View view;
    RecyclerView productList;
    ProductListService productListService;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_home, container, false);
        initialize(view);
        return view;
    }

    private void initialize(View view) {

        productList = view.findViewById(R.id.productList);
        productList.setLayoutManager(new LinearLayoutManager(getContext()));

        //SnapHelper helper = new LinearSnapHelper();
        //helper.attachToRecyclerView(productList);

        productListService = new ProductListService();

    }

    @Override
    public void onStart() {
        super.onStart();

        setHasOptionsMenu(true);

        ProgressDialogBar.getmInstance(getContext()).showDialog();

        productListService.getProductlist(getContext(), productList);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        FragmentTransaction fragmentTransaction;

        switch (itemId) {

            case R.id.cart_menu :

                CartFragment cartFragment = new CartFragment();

                fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.homeContainer, cartFragment, "CartContainer").addToBackStack("CartContainer");
                fragmentTransaction.commit();
                break;

            case  R.id.filter_menu:

                FilterFragment filterFragment = new FilterFragment();

                fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.homeContainer, filterFragment, "FilterContainer").addToBackStack("FilterContainer");
                fragmentTransaction.commit();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

}
