package tk.agsoft.ecommarce.Home.NetworkInterface;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import tk.agsoft.ecommarce.Home.request.GetProductRequest;
import tk.agsoft.ecommarce.Home.response.ProductResponse;

public interface ProductInterface {

    @POST("getProducts")
    Call<ProductResponse> getProducts(@Body GetProductRequest getProductRequest);
}
