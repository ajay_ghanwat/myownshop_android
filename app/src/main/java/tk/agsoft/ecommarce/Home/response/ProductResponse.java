package tk.agsoft.ecommarce.Home.response;

import java.util.List;

import tk.agsoft.ecommarce.DTO.ProductDTO;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class ProductResponse extends Response {

	private List<ProductDTO> products;

	public List<ProductDTO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDTO> products) {
		this.products = products;
	}

}
