package tk.agsoft.ecommarce.Home.service;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.DTO.ProductDTO;
import tk.agsoft.ecommarce.Home.adapter.ProductListAdapter;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.ProgressDialogBar;
import tk.agsoft.ecommarce.common.RunTimeComponents;
import tk.agsoft.ecommarce.Home.request.GetProductRequest;
import tk.agsoft.ecommarce.Home.response.ProductResponse;

public class ProductListService {

    private static int pageNo;

    public void getProductlist(final Context context, final RecyclerView recyclerView){

        pageNo = 0;

        GetProductRequest productRequest = new GetProductRequest();
        productRequest.setPageNO(pageNo);
        productRequest.setPageSize(10);

        Log.i("Product", "Product Request Created");

        Call<ProductResponse> responseCall = NetworkCall.getmInstance().productInterface().getProducts(productRequest);

        Log.i("Product", "Product Request ResponseCall Created");

        responseCall.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                Log.i("Product", "Got Response");
                ProgressDialogBar.getmInstance(context).hideDialog();

                if (response.body().getStatus() == ResponseStatus.OK) {
                    List<ProductDTO> productDTOList = response.body().getProducts();
                    Log.i("Product", productDTOList.toString());

                    ProductSingleToneService.getmInstance().addMultipleProductItems(response.body().getProducts());
                    recyclerView.setAdapter(new ProductListAdapter(context, ProductSingleToneService.getmInstance().getProductDTOs(), recyclerView));

                    ++pageNo;

                } else {
                    RunTimeComponents.showAlertDialogBox(context, null, response.body().getDescription());
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                Log.i("Product", "Error");
                ProgressDialogBar.getmInstance(context).hideDialog();
                RunTimeComponents.showAlertDialogBox(context, null, "Something Went Wrong!");
            }
        });

    }

}
