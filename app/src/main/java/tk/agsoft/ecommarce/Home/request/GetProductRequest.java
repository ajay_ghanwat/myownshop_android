package tk.agsoft.ecommarce.Home.request;

import tk.agsoft.ecommarce.ServerRequest.Request;

public class GetProductRequest extends Request {
	
	private Integer pageNO;
	private Integer pageSize = 1;
	private String prodCategories;
	private String prodSubCategories;

	public Integer getPageNO() {
		return pageNO;
	}
	public void setPageNO(Integer pageNO) {
		this.pageNO = pageNO;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getProdCategories() {
		return prodCategories;
	}
	public void setProdCategories(String prodCategories) {
		this.prodCategories = prodCategories;
	}
	public String getProdSubCategories() {
		return prodSubCategories;
	}
	public void setProdSubCategories(String prodSubCategories) {
		this.prodSubCategories = prodSubCategories;
	}
	
}
