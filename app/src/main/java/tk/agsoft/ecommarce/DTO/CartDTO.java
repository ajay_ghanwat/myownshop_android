package tk.agsoft.ecommarce.DTO;

public class CartDTO {

	private Integer id;
	private Integer userId;
	private ProductDTO prodId;
	private Integer prodQuantity;

	public CartDTO() {
	}

	public CartDTO(Integer id, Integer userId, ProductDTO prodId, Integer prodQuantity) {
		this.id = id;
		this.userId = userId;
		this.prodId = prodId;
		this.prodQuantity = prodQuantity;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public ProductDTO getProdId() {
		return prodId;
	}
	public void setProdId(ProductDTO prodId) {
		this.prodId = prodId;
	}
	public Integer getProdQuantity() {
		return prodQuantity;
	}
	public void setProdQuantity(Integer prodQuantity) {
		this.prodQuantity = prodQuantity;
	}
	
}
