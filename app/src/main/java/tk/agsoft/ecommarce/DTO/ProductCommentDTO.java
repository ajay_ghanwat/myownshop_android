package tk.agsoft.ecommarce.DTO;

public class ProductCommentDTO {

	private Integer id;
	private UserInfoDTO user;
	private String productCommentTitle;
	private String productCommentDesc;
	private Float productCommentRating;
	private Integer productCommentLike;
	private Integer productCommentDislike;
	private String created;
	private String updated;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UserInfoDTO getUser() {
		return user;
	}
	public void setUser(UserInfoDTO user) {
		this.user = user;
	}
	public String getProductCommentTitle() {
		return productCommentTitle;
	}
	public void setProductCommentTitle(String productCommentTitle) {
		this.productCommentTitle = productCommentTitle;
	}
	public String getProductCommentDesc() {
		return productCommentDesc;
	}
	public void setProductCommentDesc(String productCommentDesc) {
		this.productCommentDesc = productCommentDesc;
	}
	public Float getProductCommentRating() {
		return productCommentRating;
	}
	public void setProductCommentRating(Float productCommentRating) {
		this.productCommentRating = productCommentRating;
	}
	public Integer getProductCommentLike() {
		return productCommentLike;
	}
	public void setProductCommentLike(Integer productCommentLike) {
		this.productCommentLike = productCommentLike;
	}
	public Integer getProductCommentDislike() {
		return productCommentDislike;
	}
	public void setProductCommentDislike(Integer productCommentDislike) {
		this.productCommentDislike = productCommentDislike;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	
}
