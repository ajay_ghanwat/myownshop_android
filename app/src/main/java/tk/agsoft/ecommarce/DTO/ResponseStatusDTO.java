package tk.agsoft.ecommarce.DTO;

public class ResponseStatusDTO {

    private boolean isSuccessfull;

    public boolean isSuccessfull() {
        return isSuccessfull;
    }

    public void setSuccessfull(boolean successfull) {
        isSuccessfull = successfull;
    }
}
