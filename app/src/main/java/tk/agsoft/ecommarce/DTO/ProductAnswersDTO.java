package tk.agsoft.ecommarce.DTO;

public class ProductAnswersDTO {

	private Integer id;
	private UserInfoDTO user;
	private String productAns;
	private Integer productCommentLike;
	private Integer productCommentDislike;
	private String created;
	private String updated;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UserInfoDTO getUser() {
		return user;
	}
	public void setUser(UserInfoDTO user) {
		this.user = user;
	}
	public String getProductAns() {
		return productAns;
	}
	public void setProductAns(String productAns) {
		this.productAns = productAns;
	}
	public Integer getProductCommentLike() {
		return productCommentLike;
	}
	public void setProductCommentLike(Integer productCommentLike) {
		this.productCommentLike = productCommentLike;
	}
	public Integer getProductCommentDislike() {
		return productCommentDislike;
	}
	public void setProductCommentDislike(Integer productCommentDislike) {
		this.productCommentDislike = productCommentDislike;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
}
