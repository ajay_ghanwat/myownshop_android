package tk.agsoft.ecommarce.DTO;

import java.util.List;

public class WishlistDTO {

	private Integer userId;
	private List<ProductDTO> prodId;

	public WishlistDTO() {
	}

	public WishlistDTO(Integer id, Integer userId, List<ProductDTO> prodId) {
		this.userId = userId;
		this.prodId = prodId;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public List<ProductDTO> getProdId() {
		return prodId;
	}
	public void setProdId(List<ProductDTO> prodId) {
		this.prodId = prodId;
	}
	
}
