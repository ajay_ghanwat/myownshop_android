package tk.agsoft.ecommarce.DTO;

public class ProductRatingsDTO {

	private Integer id;
	private Integer fiveStar;
	private Integer fourStar;
	private Integer threeStar;
	private Integer towStar;
	private Integer oneStar;
	private String created;
	private String updated;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getFiveStar() {
		return fiveStar;
	}
	public void setFiveStar(Integer fiveStar) {
		this.fiveStar = fiveStar;
	}
	public Integer getFourStar() {
		return fourStar;
	}
	public void setFourStar(Integer fourStar) {
		this.fourStar = fourStar;
	}
	public Integer getThreeStar() {
		return threeStar;
	}
	public void setThreeStar(Integer threeStar) {
		this.threeStar = threeStar;
	}
	public Integer getTowStar() {
		return towStar;
	}
	public void setTowStar(Integer towStar) {
		this.towStar = towStar;
	}
	public Integer getOneStar() {
		return oneStar;
	}
	public void setOneStar(Integer oneStar) {
		this.oneStar = oneStar;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	
}
