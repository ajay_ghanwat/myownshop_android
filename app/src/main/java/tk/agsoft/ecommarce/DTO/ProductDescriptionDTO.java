package tk.agsoft.ecommarce.DTO;

import java.util.List;

public class ProductDescriptionDTO {

	private Integer id;
	private ProductDTO product;
	private String productName;
	private String productDescs;
	private String[] productImages;
	private String created;	
	private String updated;
	private ProductRatingsDTO ratings;
	private List<ProductCommentDTO> comments;
	private List<ProductQuestionsDTO> questions;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public ProductDTO getProduct() {
		return product;
	}
	public void setProduct(ProductDTO product) {
		this.product = product;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDescs() {
		return productDescs;
	}
	public void setProductDescs(String productDescs) {
		this.productDescs = productDescs;
	}
	public String[] getProductImages() {
		return productImages;
	}
	public void setProductImages(String[] productImages) {
		this.productImages = productImages;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	public ProductRatingsDTO getRatings() {
		return ratings;
	}
	public void setRatings(ProductRatingsDTO ratings) {
		this.ratings = ratings;
	}
	public List<ProductCommentDTO> getComments() {
		return comments;
	}
	public void setComments(List<ProductCommentDTO> comments) {
		this.comments = comments;
	}
	public List<ProductQuestionsDTO> getQuestions() {
		return questions;
	}
	public void setQuestions(List<ProductQuestionsDTO> questions) {
		this.questions = questions;
	}
	
}
