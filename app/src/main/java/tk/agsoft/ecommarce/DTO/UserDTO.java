package tk.agsoft.ecommarce.DTO;

public class UserDTO {

	private Integer id;
	private String email;
	private String password;
	private String mobileNo;
	private String userType;
	private String status;
	private String lastLogin;
	private String lastAccessTime;
	private String ipAddress;
	private String token;
	private String tokanValidity;

	public UserDTO() {
	}

	public UserDTO(Integer id, String email, String password, String mobileNo, String userType, String status, String lastLogin, String lastAccessTime, String ipAddress, String token, String tokanValidity) {
		this.id = id;
		this.email = email;
		this.password = password;
		this.mobileNo = mobileNo;
		this.userType = userType;
		this.status = status;
		this.lastLogin = lastLogin;
		this.lastAccessTime = lastAccessTime;
		this.ipAddress = ipAddress;
		this.token = token;
		this.tokanValidity = tokanValidity;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getLastAccessTime() {
		return lastAccessTime;
	}
	public void setLastAccessTime(String lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getTokanValidity() {
		return tokanValidity;
	}
	public void setTokanValidity(String tokanValidity) {
		this.tokanValidity = tokanValidity;
	}
	
}
