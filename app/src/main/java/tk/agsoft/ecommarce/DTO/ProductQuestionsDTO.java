package tk.agsoft.ecommarce.DTO;

import java.util.List;

public class ProductQuestionsDTO {

	private Integer id;
	private String productQuestion;
	private String created;
	private String updated;
	private List<ProductAnswersDTO> answers;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProductQuestion() {
		return productQuestion;
	}
	public void setProductQuestion(String productQuestion) {
		this.productQuestion = productQuestion;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	public List<ProductAnswersDTO> getAnswers() {
		return answers;
	}
	public void setAnswers(List<ProductAnswersDTO> answers) {
		this.answers = answers;
	}

}
