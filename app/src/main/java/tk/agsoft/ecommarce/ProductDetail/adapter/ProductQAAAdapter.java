package tk.agsoft.ecommarce.ProductDetail.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import tk.agsoft.ecommarce.DTO.ProductQuestionsDTO;
import tk.agsoft.ecommarce.R;

public class ProductQAAAdapter extends RecyclerView.Adapter<ProductQAAAdapter.ProductQAAHolder> {

    private Context context;
    private List<ProductQuestionsDTO> productQuestionsDTOS;

    public ProductQAAAdapter(Context context, List<ProductQuestionsDTO> productQuestionsDTOS) {
        this.context = context;
        this.productQuestionsDTOS = productQuestionsDTOS;
    }

    @NonNull
    @Override
    public ProductQAAHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_questions_ans, parent, false);
        return new ProductQAAHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductQAAHolder holder, int position) {

        ProductQuestionsDTO dto = productQuestionsDTOS.get(position);

        holder.id = dto.getId();

        holder.pqQuestion.setText(dto.getProductQuestion());

        if (!dto.getAnswers().isEmpty() && dto.getAnswers().size() > 0) {
            holder.pqAnsFound.setVisibility(View.GONE);
            holder.pqNoAnsFound.setVisibility(View.VISIBLE);

            holder.pqAns.setText(dto.getAnswers().get(0).getProductAns());
            holder.pqUser.setText(dto.getAnswers().get(0).getUser().getFirstName() + " " + dto.getAnswers().get(0).getUser().getLastName());

            if (dto.getAnswers().get(0).getUser().getStatus().equals("ACT")) {
                holder.pqStatus.setText("Certified User");
            } else {
                holder.pqStatus.setText("Not Certified User");
                holder.pqStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_indeterminate_check_box_black_24dp, 0, 0, 0);
            }

            if (dto.getAnswers().get(0).getCreated() != null) {
                SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                parser.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    Date date6 = parser.parse(dto.getAnswers().get(0).getCreated());
                    holder.pqDate.setText(date6.toGMTString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            holder.pqLikeCount.setText(dto.getAnswers().get(0).getProductCommentLike().toString());
            holder.pqdiLikeCount.setText(dto.getAnswers().get(0).getProductCommentDislike().toString());

            if (dto.getAnswers().size() > 1) {
                holder.pqAnsMore.setVisibility(View.VISIBLE);
            }

        } else {
            holder.pqAnsFound.setVisibility(View.VISIBLE);
            holder.pqNoAnsFound.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return productQuestionsDTOS.size();
    }

    class ProductQAAHolder extends RecyclerView.ViewHolder{

        Integer id;
        TextView pqQuestion, pqAns, pqUser, pqStatus, pqDate, pqLike, pqdiLike, pqLikeCount, pqdiLikeCount, pqAnsFound, pqAnsMore;
        LinearLayout pqNoAnsFound;

        ProductQAAHolder(View itemView) {
            super(itemView);
            pqQuestion = itemView.findViewById(R.id.pq_question);
            pqAns = itemView.findViewById(R.id.pq_answer);
            pqUser = itemView.findViewById(R.id.pq_user);
            pqStatus = itemView.findViewById(R.id.pq_user_status);
            pqDate = itemView.findViewById(R.id.pq_answer_date);
            pqLike = itemView.findViewById(R.id.pq_answer_like);
            pqdiLike = itemView.findViewById(R.id.pq_answer_dislike);
            pqLikeCount = itemView.findViewById(R.id.pq_answer_like_count);
            pqdiLikeCount = itemView.findViewById(R.id.pq_answer_dislike_count);
            pqAnsFound = itemView.findViewById(R.id.pq_ans_found);
            pqNoAnsFound = itemView.findViewById(R.id.pq_no_ans_found);
            pqAnsMore = itemView.findViewById(R.id.pq_ans_more);
        }
    }
}
