package tk.agsoft.ecommarce.ProductDetail.service;

import java.util.Arrays;
import java.util.List;

import tk.agsoft.ecommarce.DTO.ProductCommentDTO;
import tk.agsoft.ecommarce.DTO.ProductDescriptionDTO;
import tk.agsoft.ecommarce.DTO.ProductQuestionsDTO;

public class ProductDetailSingletonService {

    private static ProductDetailSingletonService mInstance;
    private ProductDescriptionDTO productDescriptionDTO;

    private ProductDetailSingletonService() {

        if (productDescriptionDTO == null) {
            productDescriptionDTO = new ProductDescriptionDTO();
        }
    }

    public static synchronized ProductDetailSingletonService getmInstance() {
        if (mInstance == null) {
            mInstance = new ProductDetailSingletonService();
        }

        return mInstance;
    }

    public void storeData(ProductDescriptionDTO descriptionDTO) {
        productDescriptionDTO = new ProductDescriptionDTO();

        productDescriptionDTO = descriptionDTO;
    }

    public ProductDescriptionDTO getProductDescriptionDTO() {
        return productDescriptionDTO;
    }

    public List<String> getListOfImages() {

        List<String> images = Arrays.asList(productDescriptionDTO.getProductImages());

        return images;
    }

    public List<ProductQuestionsDTO> getListOfQuestions() {
        return productDescriptionDTO.getQuestions();
    }

    public List<ProductCommentDTO> getListOfComments() {
        return productDescriptionDTO.getComments();
    }
}
