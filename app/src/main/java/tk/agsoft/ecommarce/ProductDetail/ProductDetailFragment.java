package tk.agsoft.ecommarce.ProductDetail;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import tk.agsoft.ecommarce.DTO.ProductDescriptionDTO;
import tk.agsoft.ecommarce.ProductDetail.service.ProductDetailService;
import tk.agsoft.ecommarce.ProductDetail.service.ProductDetailSingletonService;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.common.ProgressDialogBar;

public class ProductDetailFragment extends Fragment {

    View view;
    Toolbar toolbar;
    ProductDetailService detailService;
    Integer id;

    ProductDescriptionDTO descriptionDTO;

    TextView prodName, productDetailRating, productDetailPrice, productDetailFullDesc, productRatingFullContainer, productRatingDescFull;
    ProgressBar progressBarFive, progressBarFour, progressBarThree, progressBarTwo, progressBarOne;
    TextView progressBarFiveCount, progressBarFourCount, progressBarThreeCount, progressBarTwoCount, progressBarOneCount;
    public static ImageView productImagesMainContainer;
    RecyclerView productImagesContainer, allReviewsOfUser, allQuestionsOfUser;
    Button addPToCartProduct,buyNowProductPD;
    TextView totalReviewsCount, totalQuestionsCount;
    LinearLayout productreviewsShowOff,productQuestionsNoFound;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_product_detail, container, false);
        initialize(view);
        initializecomponent(view);

        Bundle bundle = getArguments();
        id = bundle.getInt("ProductId");

        return view;
    }

    private void initialize(View view) {

        toolbar = view.findViewById(R.id.main_toolbar);
        toolbar.setTitle("Product Detail");
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        detailService = new ProductDetailService();

        allReviewsOfUser = view.findViewById(R.id.allReviewsOfUser);
        allQuestionsOfUser = view.findViewById(R.id.allQuestionsOfUser);
        productImagesContainer = view.findViewById(R.id.product_images_container);

        productImagesContainer.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        allReviewsOfUser.setLayoutManager(new LinearLayoutManager(getContext()));
        allQuestionsOfUser.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    void initializecomponent(View view) {

        prodName = view.findViewById(R.id.product_full_name);
        productImagesMainContainer = view.findViewById(R.id.product_images_main_container);
        productDetailRating = view.findViewById(R.id.product_detail_rating);
        productDetailPrice = view.findViewById(R.id.product_detail_price);
        productDetailFullDesc = view.findViewById(R.id.productDetailFullDesc);
        addPToCartProduct = view.findViewById(R.id.addPToCartProduct);
        buyNowProductPD = view.findViewById(R.id.buyNowProductPD);
        productRatingFullContainer = view.findViewById(R.id.product_rating_full_container);
        productRatingDescFull = view.findViewById(R.id.product_rating_desc_full);
        progressBarFive = view.findViewById(R.id.pd_pgb_five_star);
        progressBarFour = view.findViewById(R.id.pd_pgb_four_star);
        progressBarThree = view.findViewById(R.id.pd_pgb_three_star);
        progressBarTwo = view.findViewById(R.id.pd_pgb_two_star);
        progressBarOne = view.findViewById(R.id.pd_pgb_one_star);

        progressBarFiveCount = view.findViewById(R.id.pd_pgb_five_star_count);
        progressBarFourCount = view.findViewById(R.id.pd_pgb_four_star_count);
        progressBarThreeCount = view.findViewById(R.id.pd_pgb_three_star_count);
        progressBarTwoCount = view.findViewById(R.id.pd_pgb_two_star_count);
        progressBarOneCount = view.findViewById(R.id.pd_pgb_one_star_count);
        productreviewsShowOff = view.findViewById(R.id.productreviewsShowOff);
        productQuestionsNoFound = view.findViewById(R.id.productQuestionsNoFound);

        totalReviewsCount = view.findViewById(R.id.totalReviewsCount);
        totalQuestionsCount = view.findViewById(R.id.totalQuestionsCount);
    }

    @Override
    public void onStart() {
        super.onStart();

        setHasOptionsMenu(true);

        ProgressDialogBar.getmInstance(getContext()).showDialog();

        detailService.getProductDetails(getContext(), view, id, productImagesContainer, allReviewsOfUser, allQuestionsOfUser);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home :
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setDataSetter(Context context, View view) {

        initializecomponent(view);

        descriptionDTO = ProductDetailSingletonService.getmInstance().getProductDescriptionDTO();

        if (descriptionDTO.getComments().isEmpty() || descriptionDTO.getComments().size() < 0)
            productreviewsShowOff.setVisibility(View.GONE);

        if (descriptionDTO.getQuestions().isEmpty() || descriptionDTO.getQuestions().size() < 0)
            productQuestionsNoFound.setVisibility(View.GONE);

        prodName.setText(descriptionDTO.getProductName());

        productDetailRating.setText(descriptionDTO.getProduct().getProductRating().toString());

        if(descriptionDTO.getProduct().getProductRating() == 0.0 || descriptionDTO.getProduct().getProductRating() == null) {
            productDetailRating.setBackgroundColor(context.getResources().getColor(R.color.White));
            productDetailRating.setTextColor(context.getResources().getColor(R.color.Black));
        } else if (descriptionDTO.getProduct().getProductRating() >= 0.1 && descriptionDTO.getProduct().getProductRating() <= 2.5) {
            productDetailRating.setBackground(context.getResources().getDrawable(R.drawable.red_rounded_corner));
        } else if(descriptionDTO.getProduct().getProductRating() > 2.5 && descriptionDTO.getProduct().getProductRating() <= 3.9) {
            productDetailRating.setBackground(context.getResources().getDrawable(R.drawable.yellow_rounded_corner));
        } else if(descriptionDTO.getProduct().getProductRating() >= 4.0 && descriptionDTO.getProduct().getProductRating() <= 5.0){
            productDetailRating.setBackground(context.getResources().getDrawable(R.drawable.green_rounded_corner));
        }

        productDetailPrice.setText(descriptionDTO.getProduct().getProductPrice().toString());
        productDetailFullDesc.setText(descriptionDTO.getProductDescs());

        if (descriptionDTO.getRatings().getFiveStar() == null)
            descriptionDTO.getRatings().setFiveStar(0);

        if (descriptionDTO.getRatings().getFourStar() == null)
            descriptionDTO.getRatings().setFourStar(0);

        if (descriptionDTO.getRatings().getThreeStar() == null)
            descriptionDTO.getRatings().setThreeStar(0);

        if (descriptionDTO.getRatings().getTowStar() == null)
            descriptionDTO.getRatings().setTowStar(0);

        if (descriptionDTO.getRatings().getOneStar() == null)
            descriptionDTO.getRatings().setOneStar(0);


        Integer ratings = descriptionDTO.getRatings().getFiveStar()
                    + descriptionDTO.getRatings().getFourStar()
                    + descriptionDTO.getRatings().getThreeStar()
                    + descriptionDTO.getRatings().getTowStar()
                    + descriptionDTO.getRatings().getOneStar();

        Integer ratingasPer = (ratings)/5;

        productRatingFullContainer.setText(ratingasPer.toString());
        productRatingDescFull.setText(ratings + " Retings \n " + descriptionDTO.getComments().size() + " Reviews");

        progressBarFive.setMax(ratings);
        progressBarFour.setMax(ratings);
        progressBarThree.setMax(ratings);
        progressBarTwo.setMax(ratings);
        progressBarOne.setMax(ratings);

        progressBarFive.setProgress(descriptionDTO.getRatings().getFiveStar());
        progressBarFour.setProgress(descriptionDTO.getRatings().getFourStar());
        progressBarThree.setProgress(descriptionDTO.getRatings().getThreeStar());
        progressBarTwo.setProgress(descriptionDTO.getRatings().getTowStar());
        progressBarOne.setProgress(descriptionDTO.getRatings().getOneStar());

        progressBarFiveCount.setText(descriptionDTO.getRatings().getFiveStar().toString());
        progressBarFourCount.setText(descriptionDTO.getRatings().getFourStar().toString());
        progressBarThreeCount.setText(descriptionDTO.getRatings().getThreeStar().toString());
        progressBarTwoCount.setText(descriptionDTO.getRatings().getTowStar().toString());
        progressBarOneCount.setText(descriptionDTO.getRatings().getOneStar().toString());

        if (descriptionDTO.getComments().size() > 3) {
            totalReviewsCount.setText("See All " + descriptionDTO.getComments().size() + " Reviews");
        } else {
            totalReviewsCount.setVisibility(View.GONE);
        }

        if (descriptionDTO.getQuestions().size() > 3) {
            totalQuestionsCount.setText("See All " + descriptionDTO.getQuestions().size() + " Questions And Answers");
        } else {
            totalQuestionsCount.setVisibility(View.GONE);
        }

        String[] urlImage = descriptionDTO.getProductImages();

        Picasso
                .get()
                .load(Uri.parse(urlImage[0]))
                .placeholder(R.drawable.iconfinder_32_171485)
                .error(R.drawable.ic_error_black_24dp)
                .into(productImagesMainContainer);

    }
}
