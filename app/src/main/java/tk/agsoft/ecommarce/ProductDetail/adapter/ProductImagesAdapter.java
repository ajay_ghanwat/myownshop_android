package tk.agsoft.ecommarce.ProductDetail.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import tk.agsoft.ecommarce.ProductDetail.ProductDetailFragment;
import tk.agsoft.ecommarce.R;

public class ProductImagesAdapter extends RecyclerView.Adapter<ProductImagesAdapter.ProductImagesHolder> {

    private Context context;
    private List<String> imagesList;

    public ProductImagesAdapter(Context context, List<String> imagesList) {
        this.context = context;
        this.imagesList = imagesList;
    }

    @NonNull
    @Override
    public ProductImagesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.product_image_view, parent, false);
        return new ProductImagesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductImagesHolder holder, int position) {

        String img_location = imagesList.get(position);

        holder.imageUri = img_location;

        if(!img_location.equals("") && !img_location.isEmpty()) {
            Picasso
                    .get()
                    .load(Uri.parse(img_location))
                    .placeholder(R.drawable.iconfinder_32_171485)
                    .error(R.drawable.ic_error_black_24dp)
                    .into(holder.productImgAll);
        }

        holder.productImgAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Picasso
                        .get()
                        .load(Uri.parse(holder.imageUri ))
                        .placeholder(R.drawable.iconfinder_32_171485)
                        .error(R.drawable.ic_error_black_24dp)
                        .into(ProductDetailFragment.productImagesMainContainer);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    class ProductImagesHolder extends RecyclerView.ViewHolder {

        private ImageView productImgAll;
        private String imageUri;

        ProductImagesHolder(View itemView) {
            super(itemView);

            productImgAll = itemView.findViewById(R.id.product_img_all);
        }
    }
}
