package tk.agsoft.ecommarce.ProductDetail.service;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.ProductDetail.ProductDetailFragment;
import tk.agsoft.ecommarce.ProductDetail.adapter.ProductCommentAdapter;
import tk.agsoft.ecommarce.ProductDetail.adapter.ProductImagesAdapter;
import tk.agsoft.ecommarce.ProductDetail.adapter.ProductQAAAdapter;
import tk.agsoft.ecommarce.ProductDetail.response.ProductDescResponse;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.ProgressDialogBar;
import tk.agsoft.ecommarce.common.RunTimeComponents;

public class ProductDetailService {

    public void getProductDetails(final Context context, final View view, Integer productId, final RecyclerView images, final RecyclerView comments, final RecyclerView questions) {

        Call<ProductDescResponse> responseCall = NetworkCall.getmInstance().productDetailInterface().getProductDetail(productId);

        responseCall.enqueue(new Callback<ProductDescResponse>() {
            @Override
            public void onResponse(Call<ProductDescResponse> call, Response<ProductDescResponse> response) {
                ProgressDialogBar.getmInstance(context).hideDialog();

                if (response.body().getStatus() == ResponseStatus.OK) {
                    ProductDetailSingletonService.getmInstance().storeData(response.body().getProductDescriptionDTO());

                    images.setAdapter(new ProductImagesAdapter(context, ProductDetailSingletonService.getmInstance().getListOfImages()));

                    if (ProductDetailSingletonService.getmInstance().getListOfComments().size() > 3)
                        comments.setAdapter(new ProductCommentAdapter(context, ProductDetailSingletonService.getmInstance().getListOfComments().subList(0, 2)));
                    else
                        comments.setAdapter(new ProductCommentAdapter(context, ProductDetailSingletonService.getmInstance().getListOfComments()));

                    if (ProductDetailSingletonService.getmInstance().getListOfQuestions().size() > 3)
                        questions.setAdapter(new ProductQAAAdapter(context, ProductDetailSingletonService.getmInstance().getListOfQuestions().subList(0, 2)));
                    else
                        questions.setAdapter(new ProductQAAAdapter(context, ProductDetailSingletonService.getmInstance().getListOfQuestions()));

                    new ProductDetailFragment().setDataSetter(context, view);
                } else {
                    RunTimeComponents.showAlertDialogBox(context, null, response.body().getDescription());
                }
            }

            @Override
            public void onFailure(Call<ProductDescResponse> call, Throwable t) {
                ProgressDialogBar.getmInstance(context).hideDialog();
                RunTimeComponents.showAlertDialogBox(context, null,"Something Went Wrong!");
            }
        });
    }
}
