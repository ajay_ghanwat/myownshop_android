package tk.agsoft.ecommarce.ProductDetail.response;

import tk.agsoft.ecommarce.DTO.ProductDescriptionDTO;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class ProductDescResponse extends Response {

	private ProductDescriptionDTO productDescriptionDTO;

	public ProductDescriptionDTO getProductDescriptionDTO() {
		return productDescriptionDTO;
	}

	public void setProductDescriptionDTO(ProductDescriptionDTO productDescriptionDTO) {
		this.productDescriptionDTO = productDescriptionDTO;
	}
	
}
