package tk.agsoft.ecommarce.ProductDetail.networkInterface;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import tk.agsoft.ecommarce.ProductDetail.response.ProductDescResponse;

public interface ProductDetailInterface {

    @GET("getProduct?")
    Call<ProductDescResponse> getProductDetail(@Query("requestId") Integer productID);
}
