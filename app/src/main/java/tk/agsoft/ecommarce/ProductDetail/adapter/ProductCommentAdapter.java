package tk.agsoft.ecommarce.ProductDetail.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import tk.agsoft.ecommarce.DTO.ProductCommentDTO;
import tk.agsoft.ecommarce.R;

public class ProductCommentAdapter extends RecyclerView.Adapter<ProductCommentAdapter.ProductCommentHolder> {

    private Context context;
    private List<ProductCommentDTO> commentDTOS;

    public ProductCommentAdapter(Context context, List<ProductCommentDTO> commentDTOS) {
        this.context = context;
        this.commentDTOS = commentDTOS;
    }

    @NonNull
    @Override
    public ProductCommentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_reviews_layout, parent, false);
        return new ProductCommentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductCommentHolder holder, int position) {

        ProductCommentDTO dto = commentDTOS.get(position);

        holder.id = dto.getId();

        if(dto.getProductCommentRating() == 0.0 || dto.getProductCommentRating() == null) {
            holder.pcRating.setBackgroundColor(context.getResources().getColor(R.color.White));
            holder.pcRating.setTextColor(context.getResources().getColor(R.color.Black));
        } else if (dto.getProductCommentRating() >= 0.1 && dto.getProductCommentRating() <= 2.5) {
            holder.pcRating.setBackground(context.getResources().getDrawable(R.drawable.red_rounded_corner));
        } else if(dto.getProductCommentRating() > 2.5 && dto.getProductCommentRating() <= 3.9) {
            holder.pcRating.setBackground(context.getResources().getDrawable(R.drawable.yellow_rounded_corner));
        } else if(dto.getProductCommentRating() >= 4.0 && dto.getProductCommentRating() <= 5.0){
            holder.pcRating.setBackground(context.getResources().getDrawable(R.drawable.green_rounded_corner));
        }

        holder.pcTitle.setText(dto.getProductCommentTitle());
        holder.pcDesc.setText(dto.getProductCommentDesc());
        holder.pcUser.setText(dto.getUser().getFirstName() + " " + dto.getUser().getLastName());

        if (dto.getUser().getStatus().equals("ACT"))
            holder.pcStatus.setText("Certified User");
        else
            holder.pcStatus.setText("Not Certified User");

        if(dto.getCreated() != null) {
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            parser.setTimeZone(TimeZone.getTimeZone("UTC"));
            try {
                Date date6 = parser.parse(dto.getCreated());
                holder.pcDate.setText(date6.toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        holder.pcLikeCount.setText(dto.getProductCommentLike().toString());
        holder.pcdiLikeCount.setText(dto.getProductCommentDislike().toString());
    }

    @Override
    public int getItemCount() {
        return commentDTOS.size();
    }

    class ProductCommentHolder extends RecyclerView.ViewHolder{

        Integer id;
        TextView pcRating, pcTitle, pcDesc, pcUser, pcStatus, pcDate, pcLike, pcdiLike, pcLikeCount, pcdiLikeCount;

        ProductCommentHolder(View itemView) {
            super(itemView);
            pcRating = itemView.findViewById(R.id.product_rating_comment);
            pcTitle = itemView.findViewById(R.id.product_comment_title);
            pcDesc = itemView.findViewById(R.id.product_comment_desc);
            pcUser = itemView.findViewById(R.id.product_comment_user);
            pcStatus = itemView.findViewById(R.id.product_comment_status);
            pcDate = itemView.findViewById(R.id.product_comment_time);
            pcLike = itemView.findViewById(R.id.product_comment_like_click);
            pcdiLike = itemView.findViewById(R.id.product_comment_dislike_click);
            pcLikeCount = itemView.findViewById(R.id.product_comment_like_count);
            pcdiLikeCount = itemView.findViewById(R.id.product_comment_dislike_count);
        }
    }
}
