package tk.agsoft.ecommarce.NetworkcallInterface;

import retrofit2.Call;
import retrofit2.http.GET;

public abstract class ServerInterface {

    @GET("health")
    public abstract Call<String> serverHealth();
}
