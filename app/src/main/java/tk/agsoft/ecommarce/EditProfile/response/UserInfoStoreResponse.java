package tk.agsoft.ecommarce.EditProfile.response;

import tk.agsoft.ecommarce.DTO.UserdetailInfoDTO;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class UserInfoStoreResponse extends Response {

	private UserdetailInfoDTO userDetailDTO;

	public UserdetailInfoDTO getUserDetailDTO() {
		return userDetailDTO;
	}

	public void setUserDetailDTO(UserdetailInfoDTO userDetailDTO) {
		this.userDetailDTO = userDetailDTO;
	}
	
}
