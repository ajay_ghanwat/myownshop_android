package tk.agsoft.ecommarce.EditProfile.networkInterface;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import tk.agsoft.ecommarce.EditProfile.request.UserDetailRequest;
import tk.agsoft.ecommarce.EditProfile.response.UserInfoStoreResponse;

public interface UserDataInterface {

    @POST("storeUserInfo")
    Call<UserInfoStoreResponse> storeUserInfo(@Header("User-Token") String token, @Body UserDetailRequest request);

}
