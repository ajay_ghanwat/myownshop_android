package tk.agsoft.ecommarce.EditProfile;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.DTO.UserDetailDTO;
import tk.agsoft.ecommarce.DTO.UserdetailInfoDTO;
import tk.agsoft.ecommarce.EditProfile.request.UserDetailRequest;
import tk.agsoft.ecommarce.EditProfile.response.UserInfoStoreResponse;
import tk.agsoft.ecommarce.EditProfile.service.AppLocationService;
import tk.agsoft.ecommarce.EditProfile.service.LocationAddress;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.common.DataGetterSetter;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.ProgressDialogBar;
import tk.agsoft.ecommarce.common.RunTimeComponents;

public class EditProfileFragment extends Fragment {

    View view;
    Toolbar toolbar;

    Spinner gender;
    String genderSelected;

    String[] genders = {"Male", "Female"};
    ArrayAdapter<String> genderAdapter;

    EditText fName,lName, address1, address2, city, state, pincode, country;
    static  EditText dob;

    UserDetailDTO userDetailDTO;

    Button userSaveInfo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_user_edit_profile, container, false);
        initialize(view);
        return view;
    }

    private void initialize(View view) {

        toolbar = view.findViewById(R.id.main_toolbar);
        toolbar.setTitle("Edit Profile");
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fName = view.findViewById(R.id.user_fName);
        lName = view.findViewById(R.id.user_LName);
        dob = view.findViewById(R.id.user_dob);
        address1 = view.findViewById(R.id.user_address_1);
        address2 = view.findViewById(R.id.user_address_2);
        city = view.findViewById(R.id.user_city);
        state = view.findViewById(R.id.user_state);
        pincode = view.findViewById(R.id.user_pincode);
        country = view.findViewById(R.id.user_country);
        userSaveInfo = view.findViewById(R.id.user_save_info);

        gender = view.findViewById(R.id.user_gender);

        genderAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, genders);

        gender.setAdapter(genderAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();

        setHasOptionsMenu(true);

        dob.setFocusable(false);
        dob.setClickable(true);

        userDetailDTO = DataGetterSetter.getmInstance(getContext()).getInfo();

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);
            } else {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);

            }
        } else {
            Location location = new AppLocationService(getContext())
                    .getLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                LocationAddress locationAddress = new LocationAddress();
                locationAddress.getAddressFromLocation(latitude, longitude,
                        getContext(), new GeocoderHandler());
            } else {
                showSettingsAlert();
            }
        }

        setUserInfoData(null);

    }

    private void setUserInfoData(ArrayList<String> locationAddress) {

        userDetailDTO = DataGetterSetter.getmInstance(getContext()).getInfo();

        String[] addresses = new String[2];

        String[] localAddress = null;

        fName.setText(userDetailDTO.getFirstName());
        lName.setText(userDetailDTO.getLastName());

        if (locationAddress != null) {
            if (locationAddress.get(0) != null)
                localAddress = locationAddress.get(0).split(",");
        }

        if (userDetailDTO.getAddress() != null)
            addresses = userDetailDTO.getAddress().split("\n", 2);

        if (userDetailDTO.getDob() != null  && !userDetailDTO.getDob().equals("null"))
            dob.setText(userDetailDTO.getDob());

        if (address1.getText().toString().equals("") || address1.getText().toString().isEmpty()) {
            if (!addresses[0].equals("null "))
                address1.setText(addresses[0]);
            else if (localAddress != null) {
                if (localAddress.length > 0)
                    for (int i = 0; i < localAddress.length; i++) {
                        if (i != 3)
                            address1.append(localAddress[i]+",");
                        else
                            break;
                    }
            } else {
                address1.setText("");
            }
        }

        if (address2.getText().toString().equals("") || address2.getText().toString().isEmpty()) {
            if (!addresses[1].equals(" null"))
                address2.setText(addresses[1]);
            else if (localAddress != null) {
                if (localAddress.length > 3) {
                    for (int i = 3; i < localAddress.length; i++) {
                        address2.append(localAddress[i]+",");
                    }
                }
            } else {
                address1.setText("");
            }
        }

        if (userDetailDTO.getCity() != null)
            city.setText(userDetailDTO.getCity());
        else if (locationAddress != null) {
            if (locationAddress.get(1) != null)
                city.setText(locationAddress.get(1));
        } else
            city.setText("");

        if (userDetailDTO.getState() != null)
            state.setText(userDetailDTO.getState());
        else if (locationAddress != null) {
            if (locationAddress.get(3) != null)
                state.setText(locationAddress.get(3));
        } else
            state.setText("");

        if (userDetailDTO.getPinCode() != 0)
            pincode.setText(userDetailDTO.getPinCode() + " ");
        else if (locationAddress != null) {
            if (locationAddress.get(4) != null)
                pincode.setText(locationAddress.get(4));
        } else
            pincode.setText("");

        if (userDetailDTO.getCountry() != null)
            country.setText(userDetailDTO.getCountry());
        else if (locationAddress != null) {
            if (locationAddress.get(5) != null)
                country.setText(locationAddress.get(5));
        } else
            country.setText("");

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });

        userSaveInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ProgressDialogBar.getmInstance(getContext()).showDialog();

                UserDetailRequest detailRequest = new UserDetailRequest();
                UserdetailInfoDTO infoDTO = new UserdetailInfoDTO();

                if ((fName.getText() != null || !fName.getText().toString().isEmpty() || !fName.getText().toString().equals(""))
                        && (lName.getText() != null || !lName.getText().toString().isEmpty() || !lName.getText().toString().equals(""))) {
                    infoDTO.setFirstName(fName.getText().toString());
                    infoDTO.setLastName(lName.getText().toString());
                    infoDTO.setDob(dob.getText().toString());
                    infoDTO.setGender(gender.getSelectedItem().toString());
                    infoDTO.setAddressLine1(address1.getText().toString());
                    infoDTO.setAddressLine2(address2.getText().toString());
                    infoDTO.setCity(city.getText().toString());
                    infoDTO.setCountry(country.getText().toString());
                    infoDTO.setPinCode(Integer.parseInt(pincode.getText().toString()));
                    infoDTO.setState(state.getText().toString());

                    detailRequest.setUserDetailDTO(infoDTO);
                    detailRequest.setUserId(userDetailDTO.getId());

                    Call<UserInfoStoreResponse> responseCall = NetworkCall.getmInstance().UserDataInterface().storeUserInfo(userDetailDTO.getToken(), detailRequest);

                    responseCall.enqueue(new Callback<UserInfoStoreResponse>() {
                        @Override
                        public void onResponse(Call<UserInfoStoreResponse> call, Response<UserInfoStoreResponse> response) {
                            ProgressDialogBar.getmInstance(getContext()).hideDialog();

                            if (response.body().getStatus() == ResponseStatus.OK) {

                                DataGetterSetter.getmInstance(getContext()).owerideUserInfo(response.body().getUserDetailDTO());
                                setUserInfoData(null);

                            } else {
                                RunTimeComponents.showAlertDialogBox(getContext(), null, response.body().getDescription());
                            }
                        }

                        @Override
                        public void onFailure(Call<UserInfoStoreResponse> call, Throwable t) {
                            ProgressDialogBar.getmInstance(getContext()).hideDialog();
                            RunTimeComponents.showAlertDialogBox(getContext(), null, "Something Went Wrong");
                        }
                    });
                }

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home :
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        getContext().startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            ArrayList<String> locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getStringArrayList("address");
                    break;
                default:
                    locationAddress = null;
            }
            setUserInfoData(locationAddress);
        }
    }
}
