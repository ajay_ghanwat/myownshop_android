package tk.agsoft.ecommarce.EditProfile.request;

import tk.agsoft.ecommarce.DTO.UserdetailInfoDTO;
import tk.agsoft.ecommarce.ServerRequest.Request;

public class UserDetailRequest extends Request {

    private UserdetailInfoDTO userDetailDTO;

    public UserdetailInfoDTO getUserDetailDTO() {
        return userDetailDTO;
    }

    public void setUserDetailDTO(UserdetailInfoDTO userDetailDTO) {
        this.userDetailDTO = userDetailDTO;
    }

}
