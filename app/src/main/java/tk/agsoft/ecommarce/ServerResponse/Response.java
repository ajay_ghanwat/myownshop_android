package tk.agsoft.ecommarce.ServerResponse;

import tk.agsoft.ecommarce.Constant.ResponseStatus;

public class Response {

	private String messageCode;
	private String message;
	private ResponseStatus status;
	private String description;

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ResponseStatus getStatus() {
		return status;
	}
	public void setStatus(ResponseStatus status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
