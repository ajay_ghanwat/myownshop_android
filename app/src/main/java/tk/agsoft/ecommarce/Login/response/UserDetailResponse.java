package tk.agsoft.ecommarce.Login.response;

import tk.agsoft.ecommarce.DTO.UserDetailDTO;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class UserDetailResponse extends Response {

	private UserDetailDTO userDetailDTO;

	public UserDetailDTO getUserDetailDTO() {
		return userDetailDTO;
	}

	public void setUserDetailDTO(UserDetailDTO userDetailDTO) {
		this.userDetailDTO = userDetailDTO;
	}
	
}
