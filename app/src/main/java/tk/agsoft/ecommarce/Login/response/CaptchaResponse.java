package tk.agsoft.ecommarce.Login.response;

import tk.agsoft.ecommarce.ServerResponse.Response;

public class CaptchaResponse extends Response {

	private String captchaCode;
	private String base64image;
	
	public String getCaptchaCode() {
		return captchaCode;
	}
	public void setCaptchaCode(String captchaCode) {
		this.captchaCode = captchaCode;
	}
	public String getBase64image() {
		return base64image;
	}
	public void setBase64image(String base64image) {
		this.base64image = base64image;
	}
	
}
