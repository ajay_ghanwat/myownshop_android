package tk.agsoft.ecommarce.Login.networkInterface;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import tk.agsoft.ecommarce.Login.request.ChangePasswordRequest;
import tk.agsoft.ecommarce.Login.request.ForgotPasswordRequest;
import tk.agsoft.ecommarce.Login.request.LoginDetailRequest;
import tk.agsoft.ecommarce.Login.request.RegenerateOTPRequest;
import tk.agsoft.ecommarce.Login.request.RegisterUserRequest;
import tk.agsoft.ecommarce.Login.request.UserCodeVerification;
import tk.agsoft.ecommarce.Login.request.ValidateUserRequest;
import tk.agsoft.ecommarce.ServerResponse.Response;
import tk.agsoft.ecommarce.Login.response.UserDetailResponse;
import tk.agsoft.ecommarce.Login.response.ValidUserResponse;

public interface UserInterface {

    @POST("login")
    Call<UserDetailResponse> doLogin(@Header ("Authorization") String header,@Body LoginDetailRequest loginDetailRequest);

    @POST("register")
    Call<Response> registerUser(@Body RegisterUserRequest registerUserRequest);

    @POST("forgotPassword")
    Call<Response> forgotPassword(@Body ForgotPasswordRequest forgotPasswordRequest);

    @POST("validateUser")
    Call<ValidUserResponse> validateUser(@Body ValidateUserRequest validateUserRequest);

    @POST("changepass")
    Call<Response> changepass(@Body ChangePasswordRequest changePasswordRequest);

    @POST("validateCode")
    Call<Response> validateCode(@Body UserCodeVerification userCodeVerification);

    @POST("resendCode")
    Call<Response> resendCode(@Body RegenerateOTPRequest regenerateOTPRequest);
}
