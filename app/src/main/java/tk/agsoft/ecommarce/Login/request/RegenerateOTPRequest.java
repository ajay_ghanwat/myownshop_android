package tk.agsoft.ecommarce.Login.request;

import tk.agsoft.ecommarce.ServerRequest.Request;

public class RegenerateOTPRequest extends Request {

	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
