package tk.agsoft.ecommarce.Login.request;

import tk.agsoft.ecommarce.ServerRequest.Request;

public class ValidateUserRequest extends Request {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
