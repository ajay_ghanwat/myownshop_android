package tk.agsoft.ecommarce.Login.request;

import tk.agsoft.ecommarce.ServerRequest.Request;

public class ChangePasswordRequest extends Request {

	private String username;
	private String newPass;
	private String newConfirmPass;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getNewPass() {
		return newPass;
	}
	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}
	public String getNewConfirmPass() {
		return newConfirmPass;
	}
	public void setNewConfirmPass(String newConfirmPass) {
		this.newConfirmPass = newConfirmPass;
	}
	
}
