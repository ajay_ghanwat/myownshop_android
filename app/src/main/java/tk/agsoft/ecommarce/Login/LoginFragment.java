package tk.agsoft.ecommarce.Login;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.DTO.UserDetailDTO;
import tk.agsoft.ecommarce.Login.Forgot.ForgotFragment;
import tk.agsoft.ecommarce.Login.Register.RegisterFragment;
import tk.agsoft.ecommarce.Login.request.LoginDetailRequest;
import tk.agsoft.ecommarce.Login.response.UserDetailResponse;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.common.DataGetterSetter;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.ProgressDialogBar;
import tk.agsoft.ecommarce.common.RunTimeComponents;

public class LoginFragment extends Fragment {

    View view;
    TextView forgotPass;
    Button signIn, register;
    EditText email,password;
    private static String emailPattern = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
    private static String strongRegex = "(?-i)(?=^.{8,}$)((?!.*\\s)(?=.*[A-Z])(?=.*[a-z]))(?=(1)(?=.*\\d)|.*[^A-Za-z0-9])^.*$";
    private static String validNumber = "^[0-9]{10,12}$";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_login, container, false);
        initializeAll(view);

        return view;
    }

    private void initializeAll(View view) {
        signIn = view.findViewById(R.id.sign_in);
        email = view.findViewById(R.id.user_email);
        password = view.findViewById(R.id.user_password);
        register = view.findViewById(R.id.register);
        forgotPass = view.findViewById(R.id.forgot_password);

    }

    @Override
    public void onStart() {
        super.onStart();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        signIn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_email = email.getText().toString();
                String user_password = password.getText().toString();

                if (user_email.isEmpty()) {
                    email.setError("Email or Mobile is Required!");
                } else if(!user_email.matches(emailPattern) && !user_email.matches(validNumber)) {
                    email.setError("Username Not valid!");
                }

                if (user_password.isEmpty()) {
                    password.setError("Password is Required!");
                } else if(!user_password.matches(strongRegex)){
                    password.setError("Password is Not Valid!");
                }

                if(email.getError() == null && email.getText().length() != 0) {
                    if(password.getError() == null && password.getText().length() != 0){

                        ProgressDialogBar.getmInstance(getContext()).showDialog();

                        user_email = email.getText().toString();
                        user_password = password.getText().toString();

                        String authHeader = "Basic " + Base64.encodeToString((user_email + ":" + user_password).getBytes(), Base64.NO_WRAP);

                        LoginDetailRequest detailRequest = new LoginDetailRequest();
                        detailRequest.setUsername(user_email);
                        detailRequest.setPassword(user_password);

                        Call<UserDetailResponse> response = NetworkCall.getmInstance().userInterface().doLogin(authHeader, detailRequest);

                        response.enqueue(new Callback<UserDetailResponse>() {
                            @Override
                            public void onResponse(Call<UserDetailResponse> call, Response<UserDetailResponse> response) {

                                ProgressDialogBar.getmInstance(getContext()).hideDialog();

                                if(response.body().getStatus() == ResponseStatus.OK) {

                                    UserDetailDTO userDetailDTO = response.body().getUserDetailDTO();

                                    DataGetterSetter.getmInstance(getContext()).setUserLoginData(userDetailDTO);

                                    getActivity().getSupportFragmentManager().popBackStack();

                                } else {
                                    RunTimeComponents.showAlertDialogBox(getContext(),null, response.body().getDescription());
                                }
                            }

                            @Override
                            public void onFailure(Call<UserDetailResponse> call, Throwable t) {
                                ProgressDialogBar.getmInstance(getContext()).hideDialog();
                                RunTimeComponents.showAlertDialogBox(getContext(),null, "Something Went Wrong!");
                            }
                        });
                    }
                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterFragment registerFragment = new RegisterFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.loginContainer, registerFragment).addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotFragment forgotFragment = new ForgotFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.loginContainer, forgotFragment, "LoginFragment").addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }
}
