package tk.agsoft.ecommarce.Login.Forgot;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import tk.agsoft.ecommarce.Constant.CodeType;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.ProgressDialogBar;
import tk.agsoft.ecommarce.common.RunTimeComponents;
import tk.agsoft.ecommarce.Login.request.RegenerateOTPRequest;
import tk.agsoft.ecommarce.Login.request.UserCodeVerification;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class ResetCodeFragment extends Fragment {

    View view;
    Button resetCode;
    EditText code1, code2, code3, code4;
    TextView errorCode, resendCode;
    String userName;
    private static CountDownTimer countDownTimer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_reset_code, container, false);
        initialize(view);

        Bundle bundle = getArguments();
        userName = bundle.getCharSequence("username").toString();
        return view;
    }

    private void initialize(View view) {

        resetCode = view.findViewById(R.id.su_reset_code);
        code1 = view.findViewById(R.id.code1_reset);
        code2 = view.findViewById(R.id.code2_reset);
        code3 = view.findViewById(R.id.code3_reset);
        code4 = view.findViewById(R.id.code4_reset);
        errorCode = view.findViewById(R.id.error_verify_code);
        resendCode = view.findViewById(R.id.resendCode);

    }

    @Override
    public void onStart() {
        super.onStart();

        code1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (code1.getText().toString().length() == 1) {
                    code2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        code2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (code2.getText().toString().length() == 1) {
                    code3.requestFocus();
                } else if (code2.getText().toString().length() == 0) {
                    code2.clearFocus();
                    code1.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        code3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (code3.getText().toString().length() == 1) {
                    code4.requestFocus();
                } else if (code3.getText().toString().length() == 0) {
                    code3.clearFocus();
                    code2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        code4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (code4.getText().toString().length() == 1) {
                    code4.clearFocus();
                } else if (code4.getText().toString().length() == 0) {
                    code4.clearFocus();
                    code3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        resetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(code1.getText().length() < 1 || code2.getText().length() < 1
                        || code3.getText().length() < 1 || code4.getText().length() < 1) {
                    errorCode.setVisibility(View.VISIBLE);
                } else {
                    errorCode.setVisibility(View.GONE);
                }

                if(errorCode.getVisibility() != View.VISIBLE) {

                    ProgressDialogBar.getmInstance(getContext()).showDialog();

                    String c1 = code1.getText().toString();
                    String c2 = code2.getText().toString();
                    String c3 = code3.getText().toString();
                    String c4 = code4.getText().toString();

                    String code = c1 + c2 + c3 + c4;

                    code1.setText("");
                    code2.setText("");
                    code3.setText("");
                    code4.setText("");

                    UserCodeVerification userCodeVerification = new UserCodeVerification();
                    userCodeVerification.setCode(code);
                    userCodeVerification.setCodeType(CodeType.Reset_User_Password);
                    userCodeVerification.setUserName(userName);

                    Call<Response> response = NetworkCall.getmInstance().userInterface().validateCode(userCodeVerification);

                    response.enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                            ProgressDialogBar.getmInstance(getContext()).hideDialog();

                            if(response.body().getStatus() == ResponseStatus.OK){

                                Toast.makeText(getContext(), "Account ResetCode Succcessfully!", Toast.LENGTH_SHORT).show();

                                Changepassword changepassword = new Changepassword();

                                Bundle bundle = new Bundle();
                                bundle.putCharSequence("username", userName);
                                changepassword.setArguments(bundle);

                                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.resetcodeContainer, changepassword).remove(new ResetCodeFragment());
                                fragmentTransaction.commit();

                            } else {
                                RunTimeComponents.showAlertDialogBox(getContext(),null, response.body().getDescription());
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {
                            ProgressDialogBar.getmInstance(getContext()).hideDialog();
                            RunTimeComponents.showAlertDialogBox(getContext(),null, "SomeThing Went Wrong!");

                        }
                    });

                }
            }
        });

        resendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                code1.setText("");
                code2.setText("");
                code3.setText("");
                code4.setText("");

                ProgressDialogBar.getmInstance(getContext()).showDialog();

                RegenerateOTPRequest request = new RegenerateOTPRequest();
                request.setUserName(userName);

                Call<Response> responseCall = NetworkCall.getmInstance().userInterface().resendCode(request);

                responseCall.enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        ProgressDialogBar.getmInstance(getContext()).hideDialog();

                        if(response.body().getStatus() == ResponseStatus.OK) {
                            RunTimeComponents.showAlertDialogBox(getContext(),null, response.body().getDescription());
                        } else {
                            RunTimeComponents.showAlertDialogBox(getContext(),null, response.body().getDescription());
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        ProgressDialogBar.getmInstance(getContext()).hideDialog();
                        RunTimeComponents.showAlertDialogBox(getContext(),null, "SomeThing Went Wrong!");
                    }
                });
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        startTimer(120000);
    }

    @Override
    public void onPause() {
        super.onPause();

        stopCountdown();
    }

    private void stopCountdown() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    private void startTimer(int noOfMinutes) {
        countDownTimer = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                resendCode.setVisibility(View.GONE);
            }

            public void onFinish() {

                countDownTimer = null;
                resendCode.setVisibility(View.VISIBLE);

            }
        }.start();

    }

}
