package tk.agsoft.ecommarce.Login.response;

import tk.agsoft.ecommarce.DTO.UserDTO;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class ValidUserResponse extends Response {

	private UserDTO UserDTO;

	public UserDTO getUserDTO() {
		return UserDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		UserDTO = userDTO;
	}

}
