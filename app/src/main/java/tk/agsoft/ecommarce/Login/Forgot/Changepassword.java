package tk.agsoft.ecommarce.Login.Forgot;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.ProgressDialogBar;
import tk.agsoft.ecommarce.common.RunTimeComponents;
import tk.agsoft.ecommarce.Login.request.ChangePasswordRequest;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class Changepassword extends Fragment {

    View view;
    Button submit;
    EditText userPassword, userConfirmPassword;
    String username;
    TextView changePasswordNote;

    private static String strongRegex = "(?-i)(?=^.{8,}$)((?!.*\\s)(?=.*[A-Z])(?=.*[a-z]))(?=(1)(?=.*\\d)|.*[^A-Za-z0-9])^.*$";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_change_password, container, false);
        initialize(view);

        Bundle bundle = getArguments();
        username = bundle.getCharSequence("username").toString();

        return  view;
    }

    private void initialize(View view) {

        submit = view.findViewById(R.id.submit_pass);
        userPassword = view.findViewById(R.id.user_new_password);
        userConfirmPassword = view.findViewById(R.id.user_new_password_confirm);
        changePasswordNote = view.findViewById(R.id.changePasswordNote);
    }

    @Override
    public void onStart() {
        super.onStart();

        changePasswordNote.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    changePasswordNote.setVisibility(View.VISIBLE);
                } else {
                    changePasswordNote.setVisibility(View.GONE);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(userPassword.getText().length() == 0 && !userPassword.getText().toString().matches(strongRegex)) {
                    userPassword.setError("Password Not Matches Requirement!");
                } else if(!userConfirmPassword.getText().toString().equals(userPassword.getText().toString())) {
                    userConfirmPassword.setError("Passwords Not Matched!");
                } else {

                    ProgressDialogBar.getmInstance(getContext()).showDialog();

                    ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
                    changePasswordRequest.setUsername(username);
                    changePasswordRequest.setNewPass(userPassword.getText().toString());
                    changePasswordRequest.setNewConfirmPass(userConfirmPassword.getText().toString());

                    Call<Response> responseCall = NetworkCall.getmInstance().userInterface().changepass(changePasswordRequest);

                    responseCall.enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                            ProgressDialogBar.getmInstance(getContext()).hideDialog();

                            if (response.body().getStatus() == ResponseStatus.OK) {

                                Toast.makeText(getContext(), "Password Change Successfully!", Toast.LENGTH_SHORT).show();

                                FragmentManager fm = getFragmentManager();
                                fm.popBackStackImmediate();
                            } else {
                                RunTimeComponents.showAlertDialogBox(getContext(), null, response.body().getDescription());
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {
                            ProgressDialogBar.getmInstance(getContext()).hideDialog();
                            RunTimeComponents.showAlertDialogBox(getContext(), null, "Something Went Wrong!");
                        }
                    });
                }

            }
        });
    }
}
