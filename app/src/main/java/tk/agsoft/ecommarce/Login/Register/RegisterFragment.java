package tk.agsoft.ecommarce.Login.Register;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.ProgressDialogBar;
import tk.agsoft.ecommarce.common.RunTimeComponents;
import tk.agsoft.ecommarce.Login.request.RegisterUserRequest;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class RegisterFragment extends Fragment {

    View view;
    Button register_user, reset;
    EditText fname,lname,username,upass,ucpass;
    TextView passNote;

    private static String emailPattern = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
    private static String strongRegex = "(?-i)(?=^.{8,}$)((?!.*\\s)(?=.*[A-Z])(?=.*[a-z]))(?=(1)(?=.*\\d)|.*[^A-Za-z0-9])^.*$";
    private static String validNumber = "^[0-9]{10}$";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_register, container, false);
        initialize(view);

        return view;
    }

    private void initialize(View view) {

        register_user = view.findViewById(R.id.creat_user_account);
        fname = view.findViewById(R.id.user_first_name);
        lname = view.findViewById(R.id.user_last_name);
        username = view.findViewById(R.id.user_email_id);
        upass = view.findViewById(R.id.user_reg_password);
        ucpass = view.findViewById(R.id.user_confirm_password);
        reset = view.findViewById(R.id.resetFields);
        passNote = view.findViewById(R.id.passwordNote);

    }

    @Override
    public void onStart() {
        super.onStart();

        upass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    passNote.setVisibility(View.VISIBLE);
                } else {
                    passNote.setVisibility(View.GONE);
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fname.setText("");
                lname.setText("");
                username.setText("");
                upass.setText("");
                ucpass.setText("");
            }
        });

        register_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                String user_fname = fname.getText().toString();
                String user_lname = lname.getText().toString();
                String user_email = username.getText().toString();

                if (user_fname.isEmpty()) {
                    fname.setError("first Name is Required!");
                }

                if (user_lname.isEmpty()) {
                    lname.setError("Last Name is Required!");
                }

                if (user_email.isEmpty()) {
                    username.setError("Email or Mobile is Required!");
                } else if(!user_email.matches(emailPattern) && !user_email.matches(validNumber)) {
                    username.setError("Username Not valid!");
                }

                if(upass.getText().toString().isEmpty()) {
                    upass.setError("Password Not Matches Requirement!");
                } else if( !upass.getText().toString().matches(strongRegex)) {
                    upass.setError("Password Not Matches Requirement!");
                }

                if(ucpass.getText().toString().isEmpty()) {
                    ucpass.setError("Passwords Not Matched!");
                } else if (!ucpass.getText().toString().equals(upass.getText().toString())){
                    ucpass.setError("Passwords Not Matched!");
                }

                if(fname.getError() == null && fname.getText().length() != 0) {
                    if (lname.getError() == null && lname.getText().length() != 0) {
                        if (username.getError() == null && username.getText().length() != 0) {
                            if (upass.getError() == null && upass.getText().length() != 0) {
                                if (ucpass.getError() == null && ucpass.getText().length() != 0) {

                                    ProgressDialogBar.getmInstance(getContext()).showDialog();

                                    user_fname = fname.getText().toString();
                                    user_lname = lname.getText().toString();
                                    user_email = username.getText().toString();
                                    String user_password = upass.getText().toString();
                                    String user_confirm_pass = ucpass.getText().toString();

                                    RegisterUserRequest request = new RegisterUserRequest();
                                    request.setFirstname(user_fname);
                                    request.setLastname(user_lname);
                                    request.setUsername(user_email);
                                    request.setPassword(user_password);
                                    request.setConfirmPassword(user_confirm_pass);

                                    Call<Response> response = NetworkCall.getmInstance().userInterface().registerUser(request);

                                    response.enqueue(new Callback<Response>() {
                                        @Override
                                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                                            ProgressDialogBar.getmInstance(getContext()).hideDialog();

                                            if(response.body().getStatus() == ResponseStatus.OK){

                                                RunTimeComponents.showAlertDialogBox(getContext(),null, response.body().getDescription());

                                                Bundle bundle = new Bundle();
                                                bundle.putCharSequence("username", username.getText().toString());

                                                VerifyCodeFragment verifyCodeFragment = new VerifyCodeFragment();
                                                verifyCodeFragment.setArguments(bundle);

                                                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                                fragmentTransaction.replace(R.id.registerContainer, verifyCodeFragment).remove(new RegisterFragment());
                                                fragmentTransaction.commit();

                                            } else {

                                                RunTimeComponents.showAlertDialogBox(getContext(),null, response.body().getDescription());

                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<Response> call, Throwable t) {

                                            ProgressDialogBar.getmInstance(getContext()).hideDialog();

                                            RunTimeComponents.showAlertDialogBox(getContext(),null, "Something Went Wrong!");

                                        }
                                    });

                                }
                            }
                        }
                    }
                }
            }
        });
    }
}
