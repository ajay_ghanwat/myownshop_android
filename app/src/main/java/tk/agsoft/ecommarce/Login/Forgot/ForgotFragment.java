package tk.agsoft.ecommarce.Login.Forgot;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.ProgressDialogBar;
import tk.agsoft.ecommarce.common.RunTimeComponents;
import tk.agsoft.ecommarce.Login.request.ForgotPasswordRequest;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class ForgotFragment extends Fragment {

    View view;
    Button getResetCode;
    EditText email_mobile;

    private static String emailPattern = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
    private static String validNumber = "^[0-9]{10,12}$";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_forgot, container, false);
        initiallizeAll(view);
        return view;
    }

    public void initiallizeAll(View view) {

        getResetCode = view.findViewById(R.id.reset_code);
        email_mobile = view.findViewById(R.id.user_user_name);

    }

    @Override
    public void onStart() {
        super.onStart();

        getResetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String user_name = email_mobile.getText().toString();

                if (user_name.isEmpty()) {
                    email_mobile.setError("Email or Mobile is Required!");
                } else if(!user_name.matches(emailPattern) && !user_name.matches(validNumber)) {
                    email_mobile.setError("Username Not valid!");
                }

                if(email_mobile.getError() == null && email_mobile.getText().length() != 0) {

                    ProgressDialogBar.getmInstance(getContext()).showDialog();

                    ForgotPasswordRequest request = new ForgotPasswordRequest();
                    request.setUserName(email_mobile.getText().toString());

                    RequestBody body = RequestBody.create(MediaType.parse("application/json"), request.toString());

                    Call<Response> responseCall = NetworkCall.getmInstance().userInterface().forgotPassword(request);

                    responseCall.enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                            ProgressDialogBar.getmInstance(getContext()).hideDialog();

                            if(response.body().getStatus() == ResponseStatus.OK){

                                RunTimeComponents.showAlertDialogBox(getContext(),null, response.body().getDescription());

                                ResetCodeFragment resetCodeFragment = new ResetCodeFragment();

                                Bundle bundle = new Bundle();
                                bundle.putCharSequence("username", email_mobile.getText().toString());
                                resetCodeFragment.setArguments(bundle);

                                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.forgot_password_container, resetCodeFragment).remove(new ForgotFragment());
                                fragmentTransaction.commit();

                            } else {
                                RunTimeComponents.showAlertDialogBox(getContext(),null, response.body().getDescription());
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {
                            ProgressDialogBar.getmInstance(getContext()).hideDialog();
                            RunTimeComponents.showAlertDialogBox(getContext(),null, "Something Went Wrong!");
                        }
                    });

                }
            }
        });
    }
}
