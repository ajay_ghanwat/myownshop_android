package tk.agsoft.ecommarce.Login.request;

import tk.agsoft.ecommarce.ServerRequest.Request;

public class ForgotPasswordRequest extends Request {

	private String userName;
	private String captcha;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCaptcha() {
		return captcha;
	}
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
	
}
