package tk.agsoft.ecommarce.Login.request;

import tk.agsoft.ecommarce.Constant.CodeType;
import tk.agsoft.ecommarce.ServerRequest.Request;

public class UserCodeVerification extends Request {

    private String code;
    private String userName;
    private CodeType codeType;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public CodeType getCodeType() {
        return codeType;
    }

    public void setCodeType(CodeType codeType) {
        this.codeType = codeType;
    }

}
