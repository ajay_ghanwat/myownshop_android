package tk.agsoft.ecommarce.Cart.networkInterface;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import tk.agsoft.ecommarce.Cart.request.CartRequest;
import tk.agsoft.ecommarce.Cart.response.CartResponse;

public interface CartInterface {

    @POST("getCart")
    Call<CartResponse> getCart(@Header ("User-Token") String token,@Body CartRequest cartRequest);

    @POST("addCart")
    Call<CartResponse> addCart(@Header ("User-Token") String token,@Body CartRequest cartRequest);

    @POST("leftCart")
    Call<CartResponse> leftCart(@Header ("User-Token") String token,@Body CartRequest cartRequest);

}
