package tk.agsoft.ecommarce.Cart.request;

import tk.agsoft.ecommarce.ServerRequest.Request;

public class CartRequest extends Request {

	private Integer prodQuentity;

	public Integer getProdQuentity() {
		return prodQuentity;
	}

	public void setProdQuentity(Integer prodQuentity) {
		this.prodQuentity = prodQuentity;
	}
	
}
