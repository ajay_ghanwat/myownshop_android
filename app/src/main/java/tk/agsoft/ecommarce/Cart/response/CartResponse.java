package tk.agsoft.ecommarce.Cart.response;

import java.util.List;

import tk.agsoft.ecommarce.DTO.CartDTO;
import tk.agsoft.ecommarce.ServerResponse.Response;

public class CartResponse extends Response {

	private List<CartDTO> cartlists;
	private CartDTO addedCart;

	public List<CartDTO> getCartlists() {
		return cartlists;
	}

	public void setCartlists(List<CartDTO> cartlists) {
		this.cartlists = cartlists;
	}

	public CartDTO getAddedCart() {
		return addedCart;
	}

	public void setAddedCart(CartDTO addedCart) {
		this.addedCart = addedCart;
	}

}
