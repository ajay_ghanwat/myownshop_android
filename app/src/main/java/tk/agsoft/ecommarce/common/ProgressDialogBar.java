package tk.agsoft.ecommarce.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;

import tk.agsoft.ecommarce.R;

public class ProgressDialogBar {

    private static ProgressDialogBar mInstance;
    private ProgressDialog mProgressDialog;
    private Context context;

    private ProgressDialogBar(Context context) {

        this.context = context;

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIcon(R.drawable.ic_refresh_black_24dp);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.getWindow().setLayout(50,50);
        mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mProgressDialog.getWindow().setGravity(Gravity.CENTER);
    }

    public static synchronized ProgressDialogBar getmInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ProgressDialogBar(context);
        }
        return mInstance;
    }

    public void showDialog() {

        if(mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    public void hideDialog() {

        if(mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}
