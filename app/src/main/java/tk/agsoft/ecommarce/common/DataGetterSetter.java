package tk.agsoft.ecommarce.common;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

import tk.agsoft.ecommarce.DTO.UserDetailDTO;
import tk.agsoft.ecommarce.DTO.UserdetailInfoDTO;

import static android.content.Context.MODE_PRIVATE;

public class DataGetterSetter {

    private static DataGetterSetter mInstance;
    private SharedPreferences preferences;

    private DataGetterSetter(Context context) {

        preferences = context.getSharedPreferences("ecommarce_data", MODE_PRIVATE);

    }

    public static synchronized DataGetterSetter getmInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DataGetterSetter(context);
        }
        return mInstance;
    }

    public UserDetailDTO getInfo(){

        UserDetailDTO userInfo = new UserDetailDTO();

        userInfo.setId(preferences.getInt("id", 0));
        userInfo.setFirstName(preferences.getString("firstName", null));
        userInfo.setLastName(preferences.getString("lastName", null));
        userInfo.setEmail(preferences.getString("email", null));
        userInfo.setMobileNo(preferences.getString("mobileNo", null));
        userInfo.setUserType(preferences.getString("userType",null));
        userInfo.setStatus(preferences.getString("status",null));
        userInfo.setToken(preferences.getString("token", null));
        userInfo.setTokanValidity(preferences.getString("tokanValidity",null));

        userInfo.setDob(preferences.getString("userDOB", null));
        userInfo.setGender(preferences.getString("userGender", null));
        userInfo.setPhoneNo(preferences.getString("userSecMobile", null));
        userInfo.setAddress(preferences.getString("userAddress", null));
        userInfo.setCity(preferences.getString("userCity", null));
        userInfo.setState(preferences.getString("userState", null));
        userInfo.setCountry(preferences.getString("userCountry", null));
        userInfo.setPinCode(preferences.getInt("userPinCode", 000000));

        return userInfo;
    }

    void setAllData(Map<String, String> userData, Map<String, Integer> userIntData){

        SharedPreferences.Editor editor= preferences.edit();

        if(userData != null) {

            for(int i = 0; i < userData.size(); i++){
                editor.putString(userData.get(i), userData.get(i));
                editor.apply();
            }
        }

        if(userIntData != null) {

            for(int i = 0; i < userData.size(); i++){
                editor.putInt(userData.get(i), userIntData.get(i));
                editor.apply();
            }
        }

    }

    public void setInfo(String key ,String data){

        SharedPreferences.Editor editor= preferences.edit();
        editor.putString(key, data);
        editor.apply();
    }

    public void removePerticularData(String key ,String data){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, data);
        editor.apply();
    }

    public void removeAll(){

        SharedPreferences.Editor editor = preferences.edit();

        editor.clear();
        editor.apply();
    }

    public void setSerVerData() {

        Map<String, String> serverData = new HashMap<>();

        serverData.put("server_name","localhost");
        serverData.put("server_url","192.168.0.103");
        serverData.put("server_port","8080");
        serverData.put("server_pre_url","/api/");

        setAllData(serverData, null);
    }

    public void setUserLoginData(UserDetailDTO data) {

        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt("id", data.getId());
        editor.putString("firstName", data.getFirstName());
        editor.putString("lastName", data.getLastName());
        editor.putString("email", data.getEmail());
        editor.putString("mobileNo", data.getMobileNo());
        editor.putString("userType", data.getUserType());
        editor.putString("status", data.getStatus());
        editor.putString("token", data.getToken());
        editor.putString("tokanValidity", data.getTokanValidity());

        editor.putString("userDOB", data.getDob());
        editor.putString("userGender", data.getGender());
        editor.putString("userSecMobile", data.getPhoneNo());
        editor.putString("userAddress", data.getAddress());
        editor.putString("userCity", data.getCity());
        editor.putString("userState", data.getState());
        editor.putString("userCountry", data.getCountry());
        if (data.getPinCode() != null)
            editor.putInt("userPinCode", data.getPinCode());

        editor.commit();
    }

    public void owerideUserInfo(UserdetailInfoDTO dto) {

        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("firstName", dto.getFirstName());
        editor.putString("lastName", dto.getLastName());
        editor.putString("userDOB", dto.getDob());
        editor.putString("userGender", dto.getGender());
        editor.putString("userAddress", dto.getAddressLine1() + " \n " + dto.getAddressLine2());
        editor.putString("userCity", dto.getCity());
        editor.putString("userState", dto.getState());
        editor.putString("userCountry", dto.getCountry());
        if (dto.getPinCode() != null)
            editor.putInt("userPinCode", dto.getPinCode());

        editor.commit();
    }
}
