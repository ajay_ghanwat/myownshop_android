package tk.agsoft.ecommarce.common;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import tk.agsoft.ecommarce.Cart.networkInterface.CartInterface;
import tk.agsoft.ecommarce.EditProfile.networkInterface.UserDataInterface;
import tk.agsoft.ecommarce.Home.NetworkInterface.ProductInterface;
import tk.agsoft.ecommarce.Login.networkInterface.UserInterface;
import tk.agsoft.ecommarce.ProductDetail.networkInterface.ProductDetailInterface;
import tk.agsoft.ecommarce.WishList.networkInterface.WishlistInterface;

public class NetworkCall {

    private static final String BASE_URL = "http://192.168.0.103:8080/api/";
    //private static final String BASE_URL = "http://13.127.251.203:8080/api/";
    private static NetworkCall mInstance;
    private Retrofit retrofit;

    private NetworkCall() {

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(httpLoggingInterceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(client.build())
                .build();
    }

    public static synchronized NetworkCall getmInstance() {
        if (mInstance == null) {
            mInstance = new NetworkCall();
        }
        return mInstance;
    }

    public UserInterface userInterface() {
        return retrofit.create(UserInterface.class);
    }

    public ProductInterface productInterface() {
        return retrofit.create(ProductInterface.class);
    }

    public CartInterface cartInterface() {
        return retrofit.create(CartInterface.class);
    }

    public WishlistInterface wishlistInterface() {
        return retrofit.create(WishlistInterface.class);
    }

    public ProductDetailInterface productDetailInterface() {
        return retrofit.create(ProductDetailInterface.class);
    }

    public UserDataInterface UserDataInterface() {
        return retrofit.create(UserDataInterface.class);
    }
}
