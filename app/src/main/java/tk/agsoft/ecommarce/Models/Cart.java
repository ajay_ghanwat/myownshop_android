package tk.agsoft.ecommarce.Models;

import java.util.List;

import tk.agsoft.ecommarce.DTO.ProductDTO;

public class Cart {

    private List<ProductDTO> productDTOS;

    public List<ProductDTO> getProductDTOS() {
        return productDTOS;
    }

    public void setProductDTOS(List<ProductDTO> productDTOS) {
        this.productDTOS = productDTOS;
    }
}
