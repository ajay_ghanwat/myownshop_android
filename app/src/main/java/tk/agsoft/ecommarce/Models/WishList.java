package tk.agsoft.ecommarce.Models;

import java.util.ArrayList;
import java.util.List;

import tk.agsoft.ecommarce.DTO.ProductDTO;

public class WishList {

    private List<ProductDTO> productDTOS = new ArrayList<>();

    public List<ProductDTO> getProductDTOS() {
        return productDTOS;
    }

    public void setProductDTOS(List<ProductDTO> productDTOS) {
        this.productDTOS = productDTOS;
    }
}
