package tk.agsoft.ecommarce.UserAccount;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import tk.agsoft.ecommarce.DTO.UserDetailDTO;
import tk.agsoft.ecommarce.EditProfile.EditProfileFragment;
import tk.agsoft.ecommarce.Login.LoginFragment;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.common.DataGetterSetter;

public class UserFragment extends Fragment {

    View view;
    TextView Name, PhoneNo, EmailId, addressLine1, city, pincode, state, contry, note ;
    Button edit_Profile, edit_address, userWantLoginToSee;
    RecyclerView addresses;

    LinearLayout userAccount, userwantLoginToSee;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_user_account, container, false);
        initialize(view);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private void initialize(View view) {

        Name = view.findViewById(R.id.pd_user_full_name);
        PhoneNo = view.findViewById(R.id.pd_user_mobile);
        EmailId = view.findViewById(R.id.pd_user_email);
        addressLine1 = view.findViewById(R.id.pd_user_address_1);
        city = view.findViewById(R.id.pd_user_city);
        pincode = view.findViewById(R.id.pd_user_pinCode);
        state = view.findViewById(R.id.pd_user_state);
        contry = view.findViewById(R.id.pd_user_country);
        note = view.findViewById(R.id.pd_user_address_note);

        edit_Profile = view.findViewById(R.id.pd_user_edit_profile);
        edit_address = view.findViewById(R.id.pd_user_edit_address);
        addresses = view.findViewById(R.id.pd_user_address_all);
        addresses.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        userAccount = view.findViewById(R.id.userInfoContainer);
        userwantLoginToSee = view.findViewById(R.id.userNotLoginUserAccount);

        userWantLoginToSee = view.findViewById(R.id.userWantLoginToSee);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);
            } else {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);

            }
        }

        UserDetailDTO userDetailDTO = DataGetterSetter.getmInstance(getContext()).getInfo();

        if (userDetailDTO.getToken() != null && userDetailDTO.getId() != null) {
            userAccount.setVisibility(View.VISIBLE);
            userwantLoginToSee.setVisibility(View.GONE);
        } else {
            userAccount.setVisibility(View.GONE);
            userwantLoginToSee.setVisibility(View.VISIBLE);

            userWantLoginToSee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    LoginFragment loginFragment = new LoginFragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragmentContainer, loginFragment).addToBackStack("LoginContainer");
                    fragmentTransaction.commit();
                }
            });
        }

        setUserDataToPanel();

        edit_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditProfileFragment editProfileFragment = new EditProfileFragment();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.homeContainer, editProfileFragment, "EditProfileContainer").addToBackStack("EditProfileContainer");
                fragmentTransaction.commit();
            }
        });
    }

    private void setUserDataToPanel() {

        UserDetailDTO userDetailDTO = DataGetterSetter.getmInstance(getContext()).getInfo();

        Name.setText(userDetailDTO.getFirstName() + " " + userDetailDTO.getLastName());

        if(userDetailDTO.getEmail() != null)
            EmailId.setText(userDetailDTO.getEmail());
        else
            EmailId.setText("- Add Email");

        if(userDetailDTO.getMobileNo() != null)
            PhoneNo.setText(userDetailDTO.getMobileNo());
        else
            PhoneNo.setText("- Add MobileNo");

        if(userDetailDTO.getAddress() != null && !userDetailDTO.getAddress().contains("null \n null"))
            addressLine1.setText(userDetailDTO.getAddress());
        else
            addressLine1.setText("- Add Address");

        if(userDetailDTO.getCity() != null)
            city.setText(userDetailDTO.getCity());
        else
            city.setText("- Add City");

        if(userDetailDTO.getPinCode() != 0)
            pincode.setText(userDetailDTO.getPinCode() + " ");
        else
            pincode.setText("- Add PinCode");

        if(userDetailDTO.getState() != null)
            state.setText(userDetailDTO.getState());
        else
            state.setText("- Add State");

        if(userDetailDTO.getCountry() != null)
            contry.setText(userDetailDTO.getCountry());
        else
            contry.setText("- Add Country");

    }
}
