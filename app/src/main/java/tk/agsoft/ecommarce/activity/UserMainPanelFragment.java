package tk.agsoft.ecommarce.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.DTO.UserDetailDTO;
import tk.agsoft.ecommarce.Home.HomeFragment;
import tk.agsoft.ecommarce.Login.LoginFragment;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.UserAccount.UserFragment;
import tk.agsoft.ecommarce.UserSetting.SettingFragment;
import tk.agsoft.ecommarce.WishList.WishListFragment;
import tk.agsoft.ecommarce.WishList.request.WishListRequest;
import tk.agsoft.ecommarce.WishList.response.WishlistResponse;
import tk.agsoft.ecommarce.WishList.service.WishListSingleToneService;
import tk.agsoft.ecommarce.common.DataGetterSetter;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.RunTimeComponents;

public class UserMainPanelFragment extends Fragment {

    View view;
    Toolbar toolbar;

    DrawerLayout mainMenuDrawer;
    ActionBarDrawerToggle mToggle;
    NavigationView navigationView;
    Button userLoginIn;
    TextView userName, userEmail, userMobile;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_main_container, container, false);
        initialize(view);
        return view;
    }

    private void initialize(View view) {

        toolbar = view.findViewById(R.id.main_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        navigationView = view.findViewById(R.id.main_home_navigation);

        mainMenuDrawer = view.findViewById(R.id.mainMenuDrawer);
        mToggle = new ActionBarDrawerToggle(getActivity(), mainMenuDrawer, R.string.open, R.string.close);
        mToggle.setDrawerIndicatorEnabled(true);
        mainMenuDrawer.addDrawerListener(mToggle);
        mToggle.syncState();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onStart() {
        super.onStart();

        setHasOptionsMenu(true);
        setupDrawerContent(navigationView);

        getWishListItems();

        mainMenuDrawer.closeDrawers();

        HomeFragment homeFragment = new HomeFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainUserPanel, homeFragment);
        fragmentTransaction.commit();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Home");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item))

            if (getActivity().getSupportFragmentManager().findFragmentByTag("CartContainer") != null ||
                    getActivity().getSupportFragmentManager().findFragmentByTag("FilterContainer") != null ||
                    getActivity().getSupportFragmentManager().findFragmentByTag("ProductDetailContainer") != null ||
                    getActivity().getSupportFragmentManager().findFragmentByTag("EditProfileContainer") != null) {
                getActivity().getSupportFragmentManager().popBackStackImmediate();
                mainMenuDrawer.closeDrawers();
            } else {
                return true;
            }

        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(final NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                initialize(view);
                performNavigationClick(menuItem);

                return true;
            }
        });

        userHeaderData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    public void performNavigationClick(MenuItem menuItem) {

        int itemId = menuItem.getItemId();
        Fragment fragment = null;

        switch (itemId) {

            case R.id.app_home:
                fragment = new HomeFragment();
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Home");
                break;

            case R.id.wishlist_views:
                fragment = new WishListFragment();
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("WishList");
                break;

            case R.id.user_account:
                fragment = new UserFragment();
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("User Detail");
                break;

            case R.id.user_app_setting:
                fragment = new SettingFragment();
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("User Setting");
                break;

            case R.id.user_logout:

                if (menuItem.getTitle().toString().equals("Logout")) {

                    Toast.makeText(getContext(), "Logout Successfully!", Toast.LENGTH_SHORT).show();
                    DataGetterSetter.getmInstance(getContext()).removeAll();
                    initialize(view);
                    userHeaderData();
                    WishListSingleToneService.getmInstance().clearWishList();

                    UserMainPanelFragment homeFragment = new UserMainPanelFragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragmentContainer, homeFragment);
                    fragmentTransaction.commit();

                } else if (menuItem.getTitle().toString().equals("Login")) {

                    mainMenuDrawer.closeDrawers();

                    fragment = null;
                    LoginFragment loginFragment = new LoginFragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragmentContainer, loginFragment).addToBackStack("LoginContainer");
                    fragmentTransaction.commit();
                }
                break;
        }

        if (fragment != null) {

            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mainUserPanel, fragment);
            fragmentTransaction.commit();

            mainMenuDrawer.closeDrawers();
        }
    }

    public void userHeaderData() {

        Menu menuItem1 = navigationView.getMenu();
        UserDetailDTO userDetailDTO = DataGetterSetter.getmInstance(getContext()).getInfo();

        if (userDetailDTO.getToken() != null && userDetailDTO.getId() != null) {
            menuItem1.findItem(R.id.user_logout).setTitle("Logout");
            menuItem1.findItem(R.id.user_logout).setIcon(R.drawable.ic_exit_to_app_black_24dp);
        } else {
            menuItem1.findItem(R.id.user_logout).setTitle("Login");
            menuItem1.findItem(R.id.user_logout).setIcon(R.drawable.ic_arrow_back_black_24dp);
        }

        View header = navigationView.getHeaderView(0);

        userName = header.findViewById(R.id.user_name);
        userEmail = header.findViewById(R.id.user_email_info);
        userMobile = header.findViewById(R.id.user_mobile_info);
        userLoginIn = header.findViewById(R.id.userLoginIn);

        if (userDetailDTO.getToken() != null && userDetailDTO.getId() != null) {
            userName.setText(userDetailDTO.getFirstName() + " " + userDetailDTO.getLastName());
            userEmail.setText(userDetailDTO.getEmail());
            userMobile.setText(userDetailDTO.getMobileNo());
            userLoginIn.setVisibility(View.GONE);
        } else {
            userName.setVisibility(View.GONE);
            userEmail.setVisibility(View.GONE);
            userMobile.setVisibility(View.GONE);
            userLoginIn.setVisibility(View.VISIBLE);

            userLoginIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LoginFragment loginFragment = new LoginFragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragmentContainer, loginFragment).addToBackStack("LoginContainer");
                    fragmentTransaction.commit();
                }
            });
        }

        mainMenuDrawer.closeDrawers();
    }


    private void getWishListItems() {

        UserDetailDTO userInfo = DataGetterSetter.getmInstance(getContext()).getInfo();

        if(userInfo != null) {

            if (userInfo.getToken() != null && !userInfo.getToken().isEmpty() && userInfo.getId() != null) {

                WishListRequest wishListRequest = new WishListRequest();
                wishListRequest.setUserId(userInfo.getId());

                Call<WishlistResponse> responseCall = NetworkCall.getmInstance().wishlistInterface().getWishlist(userInfo.getToken(), wishListRequest);

                responseCall.enqueue(new Callback<WishlistResponse>() {
                    @Override
                    public void onResponse(Call<WishlistResponse> call, Response<WishlistResponse> response) {

                        if (response.body().getStatus() == ResponseStatus.OK){

                            WishListSingleToneService.getmInstance().addMultipleWishList(response.body().getWishlists().getProdId());

                        } else {
                            RunTimeComponents.showAlertDialogBox(getContext(), null, response.body().getDescription());
                        }
                    }

                    @Override
                    public void onFailure(Call<WishlistResponse> call, Throwable t) {

                        RunTimeComponents.showAlertDialogBox(getContext(), null, "Something Went Wrong With WishList!");

                    }
                });

            }
        }
    }
}
