package tk.agsoft.ecommarce.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tk.agsoft.ecommarce.Constant.ResponseStatus;
import tk.agsoft.ecommarce.DTO.UserDetailDTO;
import tk.agsoft.ecommarce.R;
import tk.agsoft.ecommarce.WishList.request.WishListRequest;
import tk.agsoft.ecommarce.WishList.response.WishlistResponse;
import tk.agsoft.ecommarce.WishList.service.WishListSingleToneService;
import tk.agsoft.ecommarce.common.DataGetterSetter;
import tk.agsoft.ecommarce.common.NetworkCall;
import tk.agsoft.ecommarce.common.ProgressDialogBar;
import tk.agsoft.ecommarce.common.RunTimeComponents;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    UserDetailDTO userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/roboto_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        fragmentManager = getSupportFragmentManager();

        userInfo = DataGetterSetter.getmInstance(this).getInfo();

        if(userInfo != null) {

            if (userInfo.getToken() != null && !userInfo.getToken().isEmpty() && userInfo.getId() != null) {

                WishListRequest wishListRequest = new WishListRequest();
                wishListRequest.setUserId(userInfo.getId());

                Call<WishlistResponse> responseCall = NetworkCall.getmInstance().wishlistInterface().getWishlist(userInfo.getToken(), wishListRequest);

                responseCall.enqueue(new Callback<WishlistResponse>() {
                    @Override
                    public void onResponse(Call<WishlistResponse> call, Response<WishlistResponse> response) {

                        if (response.body().getStatus() == ResponseStatus.OK){

                            boolean isAdded = WishListSingleToneService.getmInstance().addMultipleWishList(response.body().getWishlists().getProdId());

                            if (!isAdded) {
                                RunTimeComponents.showAlertDialogBox(MainActivity.this, null, "Something Went Wrong!");
                            }

                        } else {
                            RunTimeComponents.showAlertDialogBox(MainActivity.this, null, response.body().getDescription());
                        }
                    }

                    @Override
                    public void onFailure(Call<WishlistResponse> call, Throwable t) {

                        RunTimeComponents.showAlertDialogBox(MainActivity.this, null, "Something Went Wrong With WishList!");

                    }
                });

            }
        }

        if(userInfo.getTokanValidity() != null) {
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            parser.setTimeZone(TimeZone.getTimeZone("UTC"));
            try {
                Date date6 = parser.parse(userInfo.getTokanValidity());
                if (date6.before(new Date())) {
                    DataGetterSetter.getmInstance(this).removeAll();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        UserMainPanelFragment homeFragment = new UserMainPanelFragment();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, homeFragment);
        fragmentTransaction.commit();

    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        Log.i("Count", count + "");

        if (count == 0) {
            ProgressDialogBar.getmInstance(this).hideDialog();
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}